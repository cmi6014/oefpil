/*Translated by FOR_C, v3.4.2 (-), on 07/09/115 at 08:31:19 */
/*FOR_C Options SET: ftn=u io=c no=p op=aimnv s=dbov str=l x=f - prototypes */
#include <math.h>
#include "fcrt.h"
#include "ierm1.h"
#include <stdlib.h>
void /*FUNCTION*/ ierm1(
char *subnam,
long indic,
long level,
char *msg,
char *label,
long value,
byte flag)
{
 
	/* Copyright (c) 1996 California Institute of Technology, Pasadena, CA.
	 * ALL RIGHTS RESERVED.
	 * Based on Government Sponsored Research NAS7-03001.
	 *>> 1990-01-18 CLL Added Integer stmt for VALUE.  Typed all variables.
	 *>> 1985-08-02 IERM1  Lawson  Initial code.
	 * */
	ermsg( subnam, indic, level, msg, ',' );
	ierv1( label, value, flag );
 
	return;
} /* end of function */
 
