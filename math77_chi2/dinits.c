/*Translated by FOR_C, v3.4.2 (-), on 07/09/115 at 08:31:41 */
/*FOR_C Options SET: ftn=u io=c no=p op=aimnv s=dbov str=l x=f - prototypes */
#include <math.h>
#include "fcrt.h"
#include "dinits.h"
#include <stdlib.h>
void /*FUNCTION*/ dinits(
double dos[],
long nos,
double eta,
long *nterms)
{
	long int i;
	double err;
		/* OFFSET Vectors w/subscript range: 1 to dimension */
	double *const Dos = &dos[0] - 1;
		/* end of OFFSET VECTORS */
 
	/* Copyright (c) 1996 California Institute of Technology, Pasadena, CA.
	 * ALL RIGHTS RESERVED.
	 * Based on Government Sponsored Research NAS7-03001.
	 *>> 1995-11-22 DINITS Krogh  Simplified the DO Loop.
	 *>> 1994-10-20 DINITS Krogh  Changes to use M77CON
	 *>> 1990-11-28 DINITS CLL Changed name and usage from fcn to subr.
	 *>> 1990-01-23 CLL  Corrected calls to IERM1 and DERM1.
	 *>> 1985-08-02 INITDS Lawson  Initial code.
	 *
	 * INITIALIZE THE DOUBLE PRECISION ORTHOGONAL SERIES DOS SO THAT NTERMS
	 * IS THE NUMBER OF TERMS NEEDED TO INSURE THE ERROR IS NO LARGER THAN
	 * ETA.  ORDINARILY ETA WILL BE CHOSEN TO BE ONE-TENTH MACHINE PRECISION.
	 *
	 *             --  ARGUMENTS --
	 * DOS()  [float,in]  ARRAY OF NOS COEFFICIENTS IN AN ORTHOGONAL SERIES.
	 * NOS    [integer,in]  NUMBER OF COEFFICIENTS IN DOS().
	 * ETA    [float,in]  REQUESTED ACCURACY OF SERIES.
	 * NTERMS [integer,out]  No. of terms needed to assure error .le. ETA.
	 *     ------------------------------------------------------------------
	 *--D replaces "?": ?INITS, ?ERM1
	 *     Also calls IERM1
	 *     ------------------------------------------------------------------ */
	/*     ------------------------------------------------------------------ */
	if (nos < 1)
		ierm1( "DINITS", 1, 0, "Number of coefficients < 1", "NOS"
		 , nos, '.' );
 
	err = 0.e0;
	for (i = nos; i >= 2; i--)
	{
		err += fabs( Dos[i] );
		if (err > eta)
			goto L_20;
	}
 
L_20:
	if (i == nos)
		derm1( "DINITS", 2, 0, "Requested accuracy ETA is too small."
		 , "ETA", eta, '.' );
	*nterms = i;
 
	return;
} /* end of function */
 
