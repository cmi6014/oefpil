/*Translated by FOR_C, v3.4.2 (-), on 07/09/115 at 08:30:07 */
/*FOR_C Options SET: ftn=u io=c no=p op=aimnv s=dbov str=l x=f - prototypes */
#include <math.h>
#include "fcrt.h"
#include "erfin.h"
#include <stdio.h>
#include <stdlib.h>
	/* COMMON translations */
struct t_m77err {
	long int idelta, ialpha;
	}	m77err;
	/* end of COMMON translations */
void /*FUNCTION*/ erfin()
{
 
	/* Copyright (c) 1996 California Institute of Technology, Pasadena, CA.
	 * ALL RIGHTS RESERVED.
	 * Based on Government Sponsored Research NAS7-03001.
	 *>> 1994-11-11 CLL Typing all variables.
	 *>> 1985-09-23 ERFIN  Lawson  Initial code.
	 * */
 
	printf(" $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n \n");
	if (m77err.ialpha >= 2)
		{
		exit(0);
		}
	return;
} /* end of function */
 
