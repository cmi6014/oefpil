/*Translated by FOR_C, v3.4.2 (-), on 07/09/115 at 08:30:06 */
/*FOR_C Options SET: ftn=u io=c no=p op=aimnv s=dbov str=l x=f - prototypes */
#include <math.h>
#include "fcrt.h"
#include "drexp.h"
#include <float.h>
#include <stdlib.h>
		/* PARAMETER translations */
#define	C1	(1.0e0/6.0e0)
#define	C2	(1.0e0/24.0e0)
#define	C3	(1.0e0/120.0e0)
#define	C4	(1.0e0/720.0e0)
#define	C5	(1.0e0/5040.0e0)
		/* end of PARAMETER translations */
 
double /*FUNCTION*/ drexp(
double x)
{
	long int _l0;
	double drexp_v, z;
	static double round;
	static double exmin = 1.0e0;
	static double a0 = .248015873015873015873016e-04;
	static double a1 = -.344452080605731005808147e-05;
	static double a2 = .206664230430046597475413e-06;
	static double a3 = -.447300111094328162971036e-08;
	static double a4 = .114734027080634968083920e-11;
	static double b1 = -.249994190011341852652396e00;
	static double b2 = .249987228833107957725728e-01;
	static double b3 = -.119037506846942249362528e-02;
	static double b4 = .228908693387350391768682e-04;
	static double p1 = .914041914819518e-09;
	static double p2 = .238082361044469e-01;
	static double q1 = -.499999999085958e00;
	static double q2 = .107141568980644e00;
	static double q3 = -.119041179760821e-01;
	static double q4 = .595130811860248e-03;
 
	/* Copyright (c) 1996 California Institute of Technology, Pasadena, CA.
	 * ALL RIGHTS RESERVED.
	 * Based on Government Sponsored Research NAS7-03001.
	 *>> 2001-07-16 DREXP Krogh  Change -1.0 to -1.D0.
	 *>> 1998-10-29 DREXP Krogh  Moved external statement up for mangle.
	 *>> 1994-10-20 DREXP Krogh  Changes to use M77CON
	 *>> 1994-05-20 DREXP WVS JPL Make SP and DP alike using CHGTYP
	 *>> 1993-05-06 DREXP WVS JPL Convert from NSWC to Math 77
	 *--D replaces "?": ?REXP
	 * ----------------------------------------------------------------------
	 *            EVALUATION OF THE FUNCTION EXP(X) - 1
	 * ---------------------------------------------------------------------- */
	/*     EXMIN is the smallest number such that EXP(EXMIN) - 1 > -1.
	 *     ROUND is the smallest number such that ROUND + 1 > 1. */
	/* --------------------------
	 *     CI = 1/FACTORIAL(I + 2)
	 * -------------------------- */
	/* -------------------------- */
	/* -------------------------- */
	/* ---------------------- */
	if (exmin > 0.0e0)
	{
		round = DBL_EPSILON;
		exmin = log( round );
	}
	if (fabs( x ) <= 0.15e0)
	{
		if (round < 1.0e-14)
		{
 
			/*           Z IS A MINIMAX APPROXIMATION OF THE SERIES
			 *
			 *                C6 + C7*X + C8*X**2 + ....
			 *
			 *           THIS APPROXIMATION IS ACCURATE TO WITHIN
			 *           1 UNIT OF THE 23-RD SIGNIFICANT DIGIT.
			 *           THE RESULTING VALUE FOR DREXP IS ACCURATE TO
			 *           WITHIN 1 UNIT OF THE 33-RD SIGNIFICANT
			 *           DIGIT.
			 * */
			z = ((((a4*x + a3)*x + a2)*x + a1)*x + a0)/((((b4*x +
			 b3)*x + b2)*x + b1)*x + 1.e0);
			drexp_v = x*(((((((z*x + C5)*x + C4)*x + C3)*x + C2)*x +
			 C1)*x + 0.5e0)*x + 1.e0);
			return( drexp_v );
		}
		drexp_v = x*(((p2*x + p1)*x + 1.0e0)/((((q4*x + q3)*x + q2)*
		 x + q1)*x + 1.0e0));
		return( drexp_v );
	}
 
	if (x >= 0.e0)
	{
		z = exp( x );
		drexp_v = z*(0.5e0 + (0.5e0 - 1.e0/z));
		return( drexp_v );
	}
	if (x >= exmin)
	{
		drexp_v = (exp( x ) - 0.5e0) - 0.5e0;
		return( drexp_v );
	}
	drexp_v = -1.e0;
	return( drexp_v );
} /* end of function */
 
