/*Translated by FOR_C, v3.4.2 (-), on 07/09/115 at 08:30:04 */
/*FOR_C Options SET: ftn=u io=c no=p op=aimnv s=dbov str=l x=f - prototypes */
#include <math.h>
#include "fcrt.h"
#include "dgr17.h"
#include <stdlib.h>
void /*FUNCTION*/ dgr17(
double z,
double u,
double *t)
{
	double c0, c1, c10, c2, c3, c4, c5, c6, c7, c8, c9;
	static double a0[5]={-.73324404807556026e-03,-.11758531313175796e-01,
	 -.76816029947195974e-01,-.24232172943558393e00,-.33333333333333333e00};
	static double b0[6]={.10555647473018528e-06,.73121701584237188e-03,
	 .13250270182342259e-01,.10288837674434487e00,.43024494247383254e00,
	 .97696518830675185e00};
	static double a1[4]={-.16746784557475121e-03,-.16090334014223031e-02,
	 -.52949366601406939e-02,-.18518518518518417e-02};
	static double b1[6]={.12328086517283227e-05,.98671953445602142e-03,
	 .15954049115266936e-01,.11439610256504704e00,.45195109694529839e00,
	 .98426579647613593e00};
	static double a2[4]={.12049855113125238e-04,.13743853858711134e-03,
	 .15067356806896441e-02,.41335978835983393e-02};
	static double b2[5]={.15927093345670077e-02,.22316881460606523e-01,
	 .14009848931638062e00,.50379606871703058e00,.10131761625405203e01};
	static double a3[4]={.46318872971699924e-05,.13012396979747783e-04,
	 .81804333975935872e-03,.64943415637082551e-03};
	static double b3[4]={.12414068921653593e-01,.10044290377295469e00,
	 .42226789458984594e00,.90628317147366376e00};
	static double a4[3]={-.37567394580525597e-05,-.82794205648271314e-04,
	 -.86188829773520181e-03};
	static double b4[4]={.31290397554562032e-01,.16988291247058802e00,
	 .57225859400072754e00,.10057375981227881e01};
	static double a5[2]={-.43263341886764011e-03,-.33679854644784478e-03};
	static double b5[4]={.22714615451529335e-01,.17081504060220639e00,
	 .60019022026983067e00,.10775200414676195e01};
	static double a6[2]={-.12962670089753501e-03,.53130115408837152e-03};
	static double b6[3]={.65929776650152292e-01,.45957439582639129e00,
	 .87058903334443855e00};
	static double a7[2]={.47861364421780889e-03,.34438428473168988e-03};
	static double b7[3]={.27176241899664174e00,.78991370162247144e00,
	 .12396875725833093e01};
	static double a8[2]={.27086391808339115e-03,-.65256615574219131e-03};
	static double b8[2]={.44207055629598579e00,.87002402612484571e00};
	static double a9[3]={.84725086921921823e-03,-.14838721516118744e-03,
	 -.60335050249571475e-03};
	static double a10[2]={-.19144384985654775e-02,.13324454494800656e-02};
		/* OFFSET Vectors w/subscript range: 1 to dimension */
	double *const A0 = &a0[0] - 1;
	double *const A1 = &a1[0] - 1;
	double *const A10 = &a10[0] - 1;
	double *const A2 = &a2[0] - 1;
	double *const A3 = &a3[0] - 1;
	double *const A4 = &a4[0] - 1;
	double *const A5 = &a5[0] - 1;
	double *const A6 = &a6[0] - 1;
	double *const A7 = &a7[0] - 1;
	double *const A8 = &a8[0] - 1;
	double *const A9 = &a9[0] - 1;
	double *const B0 = &b0[0] - 1;
	double *const B1 = &b1[0] - 1;
	double *const B2 = &b2[0] - 1;
	double *const B3 = &b3[0] - 1;
	double *const B4 = &b4[0] - 1;
	double *const B5 = &b5[0] - 1;
	double *const B6 = &b6[0] - 1;
	double *const B7 = &b7[0] - 1;
	double *const B8 = &b8[0] - 1;
		/* end of OFFSET VECTORS */
 
	/* Copyright (c) 1996 California Institute of Technology, Pasadena, CA.
	 * ALL RIGHTS RESERVED.
	 * Based on Government Sponsored Research NAS7-03001.
	 *>> 1994-10-20 DGR17 Krogh  Changes to use M77CON
	 *>> 1994-05-10 DGR17 WVS produce SP and DP versions from same source.
	 *>> 1993-08-04 DGR17 CLL Edited data statments for conversion to C.
	 *>> 1993-07-21 DGR17 WVS JPL Conversion from NSWC to Math 77
	 *--D replaces "?": ?GR17
	 * ----------------------------------------------------------------------
	 *
	 *            ALGORITHM USING MINIMAX APPROXIMATIONS
	 *
	 * ---------------------------------------------------------------------- */
	/* ----------------------- */
	/* ----------------------- */
	/* ----------------------- */
	/* ----------------------- */
	/* ----------------------- */
	/* ----------------------- */
	/* ----------------------- */
	/* ----------------------- */
	/* ----------------------- */
	/* ----------------------- */
	/* ----------------------- */
	/* -----------------------
	 * */
	c0 = ((((A0[1]*z + A0[2])*z + A0[3])*z + A0[4])*z + A0[5])/((((((B0[1]*
	 z + B0[2])*z + B0[3])*z + B0[4])*z + B0[5])*z + B0[6])*z + 1.0e0);
	c1 = (((A1[1]*z + A1[2])*z + A1[3])*z + A1[4])/((((((B1[1]*z +
	 B1[2])*z + B1[3])*z + B1[4])*z + B1[5])*z + B1[6])*z + 1.0e0);
	c2 = (((A2[1]*z + A2[2])*z + A2[3])*z + A2[4])/(((((B2[1]*z +
	 B2[2])*z + B2[3])*z + B2[4])*z + B2[5])*z + 1.0e0);
	c3 = (((A3[1]*z + A3[2])*z + A3[3])*z + A3[4])/((((B3[1]*z + B3[2])*
	 z + B3[3])*z + B3[4])*z + 1.0e0);
	c4 = ((A4[1]*z + A4[2])*z + A4[3])/((((B4[1]*z + B4[2])*z + B4[3])*
	 z + B4[4])*z + 1.0e0);
	c5 = (A5[1]*z + A5[2])/((((B5[1]*z + B5[2])*z + B5[3])*z + B5[4])*
	 z + 1.0e0);
	c6 = (A6[1]*z + A6[2])/(((B6[1]*z + B6[2])*z + B6[3])*z + 1.0e0);
	c7 = (A7[1]*z + A7[2])/(((B7[1]*z + B7[2])*z + B7[3])*z + 1.0e0);
	c8 = (A8[1]*z + A8[2])/((B8[1]*z + B8[2])*z + 1.0e0);
	c9 = (A9[1]*z + A9[2])*z + A9[3];
	c10 = A10[1]*z + A10[2];
 
	*t = (((((((((c10*u + c9)*u + c8)*u + c7)*u + c6)*u + c5)*u +
	 c4)*u + c3)*u + c2)*u + c1)*u + c0;
 
	return;
} /* end of function */
 
