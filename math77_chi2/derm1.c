/*Translated by FOR_C, v3.4.2 (-), on 07/09/115 at 08:31:14 */
/*FOR_C Options SET: ftn=u io=c no=p op=aimnv s=dbov str=l x=f - prototypes */
#include <math.h>
#include "fcrt.h"
#include "derm1.h"
#include <stdlib.h>
void /*FUNCTION*/ derm1(
char *subnam,
long indic,
long level,
char *msg,
char *label,
double value,
byte flag)
{
 
	/* Copyright (c) 1996 California Institute of Technology, Pasadena, CA.
	 * ALL RIGHTS RESERVED.
	 * Based on Government Sponsored Research NAS7-03001.
	 *>> 1994-10-20 DERM1  Krogh  Changes to use M77CON
	 *>> 1994-04-20 DERM1  CLL Edited to make DP & SP files similar.
	 *>> 1985-08-02 DERM1  Lawson  Initial code.
	 *--D replaces "?": ?ERM1, ?ERV1
	 * */
 
	ermsg( subnam, indic, level, msg, ',' );
	derv1( label, value, flag );
 
	return;
} /* end of function */
 
