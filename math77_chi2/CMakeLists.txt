add_library(math77_chi2 OBJECT
dcdchi.c
dcsevl.c
derf.c
derm1.c
derv1.c
dgam1.c
dgami.c
dgamma.c
dgr17.c
dgr29.c
dinits.c
drcomp.c
drexp.c
drlog.c
dxparg.c
erfin.c
ermsg.c
fhelp.c
ierm1.c
ierv1.c
)

# avoid gcc errors due to multiple definition of "m77err"
target_compile_options(math77_chi2 PRIVATE -fcommon)