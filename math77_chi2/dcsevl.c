/*Translated by FOR_C, v3.4.2 (-), on 07/09/115 at 08:31:40 */
/*FOR_C Options SET: ftn=u io=c no=p op=aimnv s=dbov str=l x=f - prototypes */
#include <math.h>
#include "fcrt.h"
#include "dcsevl.h"
#include <stdlib.h>
double /*FUNCTION*/ dcsevl(
double x,
double a[],
long n)
{
	long int i;
	double b0, b1, b2, dcsevl_v, twox;
		/* OFFSET Vectors w/subscript range: 1 to dimension */
	double *const A = &a[0] - 1;
		/* end of OFFSET VECTORS */
 
	/* Copyright (c) 1996 California Institute of Technology, Pasadena, CA.
	 * ALL RIGHTS RESERVED.
	 * Based on Government Sponsored Research NAS7-03001.
	 *>> 1994-10-20 DCSEVL Krogh  Changes to use M77CON
	 *>> 1990-11-29 DCSEVL CLL Changing name of single precision version.
	 *>> 1989-10-30 CLL
	 *>> 1985-08-02 DCSEVL Lawson  Initial code.
	 *
	 * EVALUATE THE N-TERM CHEBYSHEV SERIES A AT X. ADAPTED FROM
	 * R. BROUCKE, ALGORITHM 446, C.A.C.M., 16, 254 (1973).
	 * W. FULLERTON, C-3, LOS ALAMOS SCIENTIFIC LABORATORY.
	 * INSTALLED ON THE VAX BY DOLORES MONTANO, C-3, 5/80.
	 * 1989-10-30 CLL. Replaced name CSEVL with DCSEVL in calls to error
	 *     processing subroutines.  Commented out the test on X being in
	 *     [-1.0, 1.0].  Was giving unnecessary error msg when X was
	 *     slightly greater than 1.0 due to imprecise Cray DP arithmetic.
	 *
	 *       INPUT ARGUMENTS --
	 * X  [float,in]  VALUE AT WHICH THE SERIES IS TO BE EVALUATED.
	 * A  [float,in]  ARRAY OF N TERMS OF A CHEBYSHEV SERIES. IN EVAL-
	 *      UATING the series, ONLY HALF THE FIRST COEF IS SUMMED.
	 * N  [integer,in]  NUMBER OF TERMS IN ARRAY A.
	 *     ------------------------------------------------------------------
	 *--D replaces "?": ?CSEVL, ?ERM1
	 *     Also uses IERM1
	 *     ------------------------------------------------------------------ */
	/*     ------------------------------------------------------------------ */
	if (n < 1)
		ierm1( "DCSEVL", 1, 0, "NUMBER OF TERMS .LE. 0", "ARG", n,
		 '.' );
	if (n > 1000)
		ierm1( "DCSEVL", 2, 0, "NUMBER OF TERMS .GT. 1000", "ARG",
		 n, '.' );
	/*     IF (X .LT. -1.D0 .OR. X .GT. 1.D0) CALL DERM1(
	 *    *'DCSEVL',3,0,'X OUTSIDE (-1,+1)','ARG',X,'.')
	 *
	 * */
	twox = 2.0e0*x;
	b1 = 0.e0;
	b0 = 0.e0;
	for (i = n; i >= 1; i--)
	{
		b2 = b1;
		b1 = b0;
		b0 = twox*b1 - b2 + A[i];
	}
 
	dcsevl_v = 0.5e0*(b0 - b2);
 
	return( dcsevl_v );
} /* end of function */
 
