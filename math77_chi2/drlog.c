/*Translated by FOR_C, v3.4.2 (-), on 07/09/115 at 08:30:06 */
/*FOR_C Options SET: ftn=u io=c no=p op=aimnv s=dbov str=l x=f - prototypes */
#include <math.h>
#include "fcrt.h"
#include "drlog.h"
#include <float.h>
#include <stdlib.h>
double /*FUNCTION*/ drlog(
double x)
{
	double drlog_v, r;
 
	/* Copyright (c) 1996 California Institute of Technology, Pasadena, CA.
	 * ALL RIGHTS RESERVED.
	 * Based on Government Sponsored Research NAS7-03001.
	 *  File drlog contains user callable procedures drlog & drlog1, and
	 *               lower level procedure drlog2.
	 *  drlog(x)  computes x - 1 - ln(x).
	 *  drlog1(x) computes x - ln(1 + x).
	 *>> 1996-03-30 DRLOG  Krogh  Added external statements.
	 *>> 1994-11-09 CLL Edited to avoid ENTRY statement.
	 *>> 1994-10-20 DRLOG Krogh  Changes to use M77CON
	 *>> 1994-05-23 DRLOG WVS JPL Combine DRLOG and DRLOG1
	 *>> 1994-05-23 DRLOG WVS JPL Make SP and DP alike using CHGTYP
	 *>> 1993-05-06 DRLOG WVS JPL Conversion from NSWC to Math 77
	 * ----------------------------------------------------------------------
	 *--D replaces "?": ?RLOG, ?RLOG1, ?RLOG2
	 *     ==================================================================
	 *             EVALUATION OF THE FUNCTION X - 1 - LN(X)
	 *     ------------------------------------------------------------------ */
	/*     ------------------------------------------------------------------ */
	r = (x - 0.5e0) - 0.5e0;
	if (x < 0.61e0 || x > 1.57e0)
	{
		drlog_v = r - log( x );
	}
	else
	{
		drlog_v = drlog2( r );
	}
	return( drlog_v );
} /* end of function */
/*     ================================================================== */
double /*FUNCTION*/ drlog1(
double x)
{
	double drlog1_v, r;
 
	/*             EVALUATION OF THE FUNCTION X - LN(1 + X)
	 *     ------------------------------------------------------------------ */
	/*     ------------------------------------------------------------------ */
	if (x < -0.39e0 || x > 0.57e0)
	{
		r = (x + 0.5e0) + 0.5e0;
		drlog1_v = x - log( r );
	}
	else
	{
		drlog1_v = drlog2( x );
	}
	return( drlog1_v );
} /* end of function */
/*     ================================================================== */
		/* PARAMETER translations */
#define	C1	(1.0e0/3.0e0)
#define	C2	(1.0e0/5.0e0)
#define	C3	(1.0e0/7.0e0)
#define	C4	(1.0e0/9.0e0)
#define	C5	(1.0e0/11.0e0)
		/* end of PARAMETER translations */
 
double /*FUNCTION*/ drlog2(
double rin)
{
	long int _l0;
	double drlog2_v, r, t, u, up2, w, z;
	static double a = .566749439387323789126387112411845e-01;
	static double b = .456512608815524058941143273395059e-01;
	static double p0 = .7692307692307692307680e-01;
	static double p1 = -.1505958055914600184836e00;
	static double p2 = .9302355725278521726994e-01;
	static double p3 = -.1787900022182327735804e-01;
	static double q1 = -.2824412139355646910683e01;
	static double q2 = .2892424216041495392509e01;
	static double q3 = -.1263560605948009364422e01;
	static double q4 = .1966769435894561313526e00;
	static double r0 = .333333333333333e00;
	static double r1 = -.224696413112536e00;
	static double r2 = .620886815375787e-02;
	static double s1 = -.127408923933623e01;
	static double s2 = .354508718369557e00;
	static double round = -1.0e0;
 
	/*            Complete computation started by DRLOG or DRLOG1.
	 *     ------------------------------------------------------------------ */
	/* ------------------------
	 *     CI = 1/(2I + 1)
	 * ------------------------ */
	/* ------------------------
	 *     A = DRLOG (0.7)
	 *     B = DRLOG (4/3)
	 * ------------------------ */
	/* ------------------------ */
	/* ------------------------ */
	/* ------------------------ */
	/*     ------------------------------------------------------------------
	 *
	 *                 ARGUMENT REDUCTION
	 * */
	r = rin;
	if (r < -0.18e0)
	{
		u = (10.0e0*r + 3.0e0)/7.0e0;
		up2 = u + 2.0e0;
		w = a - u*0.3e0;
	}
	else if (r > 0.18e0)
	{
		t = 0.75e0*r;
		u = t - 0.25e0;
		up2 = t + 1.75e0;
		w = b + u/3.0e0;
	}
	else
	{
		u = r;
		up2 = u + 2.0e0;
		w = 0.0e0;
	}
	if (round < 0.0e0)
		round = DBL_EPSILON;
 
	/*                  SERIES EXPANSION
	 * */
	r = u/up2;
	t = r*r;
	if (round > 5.0e-14)
	{
		z = ((r2*t + r1)*t + r0)/((s2*t + s1)*t + 1.0e0);
	}
	else
	{
 
		/*        Z IS (AT FIRST) A MINIMAX APPROXIMATION OF THE SERIES
		 *
		 *               C6 + C7*R**2 + C8*R**4 + ...
		 *
		 *        FOR THE INTERVAL (0.0, 0.375). THE APPROXIMATION IS ACCURATE
		 *        TO WITHIN 1.6 UNITS OF THE 21-ST SIGNIFICANT DIGIT.
		 * */
		z = (((p3*t + p2)*t + p1)*t + p0)/((((q4*t + q3)*t + q2)*t +
		 q1)*t + 1.0e0);
 
		z = ((((z*t + C5)*t + C4)*t + C3)*t + C2)*t + C1;
	}
	drlog2_v = r*(u - 2.0e0*t*z) + w;
	return( drlog2_v );
} /* end of function */
 
