/*Translated by FOR_C, v3.4.2 (-), on 07/09/115 at 08:30:07 */
/*FOR_C Options SET: ftn=u io=c no=p op=aimnv s=dbov str=l x=f - prototypes */
#include <math.h>
#include "fcrt.h"
#include "dxparg.h"
#include <float.h>
#include <stdlib.h>
double /*FUNCTION*/ dxparg(
long l)
{
	long int _l0;
	double dxparg_v, fac;
	static double xmin;
	static double xmax = -1.0e0;
 
	/* Copyright (c) 1996 California Institute of Technology, Pasadena, CA.
	 * ALL RIGHTS RESERVED.
	 * Based on Government Sponsored Research NAS7-03001.
	 *>> 1998-10-29 DXPARG Krogh  Moved external statement up for mangle.
	 *>> 1994-10-20 DXPARG Krogh  Changes to use M77CON
	 *>> 1993-05-06 DXPARG WVS JPL Conversion from NSWC to Math 77
	 *--D replaces "?": ?XPARG
	 * -------------------------------------------------------------------
	 *     IF L = 0 THEN  DXPARG(L) = THE LARGEST POSITIVE W FOR WHICH
	 *     EXP(W) CAN BE COMPUTED.
	 *
	 *     IF L IS NONZERO THEN  DXPARG(L) = THE LARGEST NEGATIVE W FOR
	 *     WHICH THE COMPUTED VALUE OF EXP(W) IS NONZERO.
	 *
	 *     NOTE... ONLY AN APPROXIMATE VALUE FOR DXPARG(L) IS NEEDED.
	 * ------------------------------------------------------------------- */
 
	if (xmax < 0.0)
	{
		fac = 1.0e0 - DBL_EPSILON/FLT_RADIX;
		xmax = fac*log( DBL_MAX );
		xmin = fac*log( DBL_MIN );
	}
 
	if (l != 0)
	{
		dxparg_v = xmin;
	}
	else
	{
		dxparg_v = xmax;
	}
	return( dxparg_v );
} /* end of function */
 
