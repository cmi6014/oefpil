/*Translated by FOR_C, v3.4.2 (-), on 07/09/115 at 08:30:54 */
/*FOR_C Options SET: ftn=u io=c no=p op=aimnv s=dbov str=l x=f - prototypes */
#include <math.h>
#include "fcrt.h"
#include "ermsg.h"
#include <stdio.h>
#include <stdlib.h>
	/* COMMON translations */
struct t_m77err {
	long int idelta, ialpha;
	}	m77err;
	/* end of COMMON translations */
void /*FUNCTION*/ ermsg(
char *subnam,
long indic,
long level,
char *msg,
byte flag)
{
	static long ideloc = 0;
 
	/* Copyright (c) 1996 California Institute of Technology, Pasadena, CA.
	 * ALL RIGHTS RESERVED.
	 * Based on Government Sponsored Research NAS7-03001.
	 *>> 1995-11-22 ERMSG  Krogh Got rid of multiple entries.
	 *>> 1995-09-15 ERMSG  Krogh Remove '0' in format.
	 *>> 1994-11-11 ERMSG  Krogh   Declared all vars.
	 *>> 1992-10-20 ERMSG  WV Snyder  added ERLSET, ERLGET
	 *>> 1985-09-25 ERMSG  Lawson  Initial code.
	 *
	 *     --------------------------------------------------------------
	 *
	 *     Four entries: ERMSG, ERMSET, ERLGET, ERLSET
	 *     ERMSG initiates an error message. This subr also manages the
	 *     saved value IDELOC and the saved COMMON block M77ERR to
	 *     control the level of action. This is intended to be the
	 *     only subr that assigns a value to IALPHA in COMMON.
	 *     ERMSET resets IDELOC & IDELTA.  ERLGET returns the last value
	 *     of LEVEL passed to ERMSG.  ERLSET sets the last value of LEVEL.
	 *     ERLSET and ERLGET may be used together to determine the level
	 *     of error that occurs during execution of a routine that uses
	 *     ERMSG.
	 *
	 *     --------------------------------------------------------------
	 *     SUBROUTINE ARGUMENTS
	 *     --------------------
	 *     SUBNAM   A name that identifies the subprogram in which
	 *              the error occurs.
	 *
	 *     INDIC    An integer printed as part of the mininal error
	 *              message. It together with SUBNAM can be used to
	 *              uniquely identify an error.
	 *
	 *     LEVEL    The user sets LEVEL=2,0,or -2 to specify the
	 *              nominal action to be taken by ERMSG. The
	 *              subroutine ERMSG contains an internal variable
	 *              IDELTA, whose nominal value is zero. The
	 *              subroutine will compute IALPHA = LEVEL + IDELTA
	 *              and proceed as follows:
	 *              If (IALPHA.GE.2)        Print message and STOP.
	 *              If (IALPHA=-1,0,1)      Print message and return.
	 *              If (IALPHA.LE.-2)       Just RETURN.
	 *
	 *     MSG      Message to be printed as part of the diagnostic.
	 *
	 *     FLAG     A single character,which when set to '.' will
	 *              call the subroutine ERFIN and will just RETURN
	 *              when set to any other character.
	 *
	 *     --------------------------------------------------------------
	 *
	 *     C.Lawson & S.Chan, JPL, 1983 Nov
	 *
	 *     ------------------------------------------------------------------ */
 
	if (level < -1000)
	{
		/*                            Setting a new IDELOC. */
		m77err.idelta = level + 10000;
		ideloc = m77err.idelta;
		return;
	}
	m77err.idelta = ideloc;
	m77err.ialpha = level + m77err.idelta;
	if (m77err.ialpha >= -1)
	{
 
		/*            Setting FILE = 'CON' works for MS/DOS systems.
		 *
		 * */
		printf(" \n $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n SUBPROGRAM %s REPORTS ERROR NO. %4ld\n",
		   subnam, indic);
		printf("%s \n", msg);
		if (flag == '.')
			erfin();
	}
	return;
 
} /* end of function */
 
void /*FUNCTION*/ ermset(
long idel)
{
 
	/*                            Call ERMSG to set IDELTA and IDELOC */
	ermsg( " ", 0, idel - 10000, " ", ' ' );
	return;
} /* end of function */
 
