/*Translated by FOR_C, v3.4.2 (-), on 07/09/115 at 08:30:54 */
/*FOR_C Options SET: ftn=u io=c no=p op=aimnv s=dbov str=l x=f - prototypes */
#include <math.h>
#include "fcrt.h"
#include "ierv1.h"
#include <stdio.h>
#include <stdlib.h>
	/* COMMON translations */
struct t_m77err {
	long int idelta, ialpha;
	}	m77err;
	/* end of COMMON translations */
void /*FUNCTION*/ ierv1(
char *label,
long value,
byte flag)
{
 
	/* Copyright (c) 1996 California Institute of Technology, Pasadena, CA.
	 * ALL RIGHTS RESERVED.
	 * Based on Government Sponsored Research NAS7-03001.
	 *>> 1995-11-15 IERV1 Krogh  Moved format up for C conversion.
	 *>> 1985-09-20 IERV1  Lawson  Initial code.
	 *
	 *     ------------------------------------------------------------
	 *     SUBROUTINE ARGUMENTS
	 *     --------------------
	 *     LABEL     An identifing name to be printed with VALUE.
	 *
	 *     VALUE     A integer to be printed.
	 *
	 *     FLAG      See write up for FLAG in ERMSG.
	 *
	 *     ------------------------------------------------------------
	 * */
 
	if (m77err.ialpha >= -1)
	{
		printf("   %s = %5ld\n", label, value);
		if (flag == '.')
			erfin();
	}
	return;
 
} /* end of function */
 
