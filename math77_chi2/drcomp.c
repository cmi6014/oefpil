/*Translated by FOR_C, v3.4.2 (-), on 07/09/115 at 08:32:03 */
/*FOR_C Options SET: ftn=u io=c no=p op=aimnv s=dbov str=l x=f - prototypes */
#include <math.h>
#include "fcrt.h"
#include "drcomp.h"
#include <float.h>
#include <stdlib.h>
		/* PARAMETER translations */
#define	RT2PIN	.398942280401432677939946059934e0
		/* end of PARAMETER translations */
 
double /*FUNCTION*/ drcomp(
double a,
double x)
{
	long int _l0;
	double drcomp_v, t, u;
	static double emin;
	static long nterms = 0;
	static double c[30]={1.6666674603806317936070839523784458533810e-1,
	 -0.27778224326356936403488508628449409187e-4,0.39686751449471417966822710527999178e-7,
	 -0.148869543950843614683173607368649e-9,0.1053394436947088748911134694152e-11,
	 -0.12017062666093330302702077565e-13,0.201455314614512105435465085e-15,
	 -0.4667111367329679717841157e-17,0.143038901715103888195513e-18,
	 -0.5615023733620632617039e-20,0.275468595656664717191e-21,-0.16574673341842014242e-22,
	 0.1205878144005562980e-23,-0.105029344505420423e-24,0.10888441731031588e-25,
	 -0.1345007495569099e-26,0.198172466731588e-27,-0.35717869246269e-28,
	 0.7475856209303e-29,-0.1991899701570e-29,0.510170672127e-30,-0.177502712854e-30,
	 0.43613516909e-31,-0.17176901844e-31,0.3387002917e-32,-0.1399833946e-32,
	 0.188634194e-33,-0.79096000e-34,0.5378442e-35,-0.2252006e-35};
		/* OFFSET Vectors w/subscript range: 1 to dimension */
	double *const C = &c[0] - 1;
		/* end of OFFSET VECTORS */
 
	/* Copyright (c) 1996 California Institute of Technology, Pasadena, CA.
	 * ALL RIGHTS RESERVED.
	 * Based on Government Sponsored Research NAS7-03001.
	 *>> 2001-06-18 DRCOMP Krogh  Removed blanks inside floating pt. nums.
	 *>> 1998-10-29 DRCOMP Krogh  Moved external statement up for mangle.
	 *>> 1994-11-08 CLL Edited data stmt for conversion to C.
	 *>> 1994-10-20 DRCOMP Krogh  Changes to use M77CON
	 *>> 1992-05-06 DRCOMP WVS JPL Conversion from NSWC to Math 77
	 *--D replaces "?": ?CSEVL, ?GAMMA, ?GAM1, ?INITS, ?RCOMP
	 *--   &              ?RLOG, ?XPARG
	 * ----------------------------------------------------------------------
	 *                EVALUATION OF EXP(-X)*X**A/GAMMA(A)
	 * ----------------------------------------------------------------------
	 *     RT2PIN = 1/SQRT(2*PI)
	 * ------------------------- */
	/* ------------------------- */
	/*     Coefficients for a 30 term truncated Chebyshev series for the
	 *     tail of the Stirling approximation for log gamma (a), i.e.
	 *     10/a*sum(bernoulli(2*m)/((2*m)*(2*m-1))*(10/a)^(2*m),m=1..30): */
 
	drcomp_v = 0.0e0;
	if (x == 0.0e0)
		return( drcomp_v );
	if (a < 20.0e0)
	{
 
		t = a*log( x ) - x;
		if (t < dxparg( 1 ))
			return( drcomp_v );
		if (a < 1.0e0)
		{
			drcomp_v = (a*exp( t ))*(1.0e0 + dgam1( a ));
			return( drcomp_v );
		}
		drcomp_v = exp( t )/dgamma( a );
	}
	else
	{
		u = x/a;
		if (u == 0.0e0)
			return( drcomp_v );
		if (nterms == 0)
		{
			dinits( c, 30, 0.1e0*DBL_EPSILON, &nterms );
			emin = dxparg( 1 );
		}
		t = -(dcsevl( powi(10.0e0/a,2), c, nterms )/a + a*drlog( u ));
		if (t >= emin)
			drcomp_v = RT2PIN*sqrt( a )*exp( t );
	}
	return( drcomp_v );
} /* end of function */
 
