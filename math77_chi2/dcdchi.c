/*Translated by FOR_C, v3.4.2 (-), on 07/09/115 at 08:32:08 */
/*FOR_C Options SET: ftn=u io=c no=p op=aimnv s=dbov str=l x=f - prototypes */
#include <math.h>
#include "fcrt.h"
#include "dcdchi.h"
#include <stdlib.h>
void /*FUNCTION*/ dcdchi(
double chisq,
double nu,
double *p,
double *q,
long *ierr)
{
 
	/* Copyright (c) 1996 California Institute of Technology, Pasadena, CA.
	 * ALL RIGHTS RESERVED.
	 * Based on Government Sponsored Research NAS7-03001.
	 *>> 1994-10-20 DCDCHI Krogh  Changes to use M77CON
	 *>> 1994-08-05 DCDCHI WV Snyder
	 *
	 *     Compute the Cumulative Density Function of the Chi-squared
	 *     distribution for parameter argument CHISQ and NU degrees of
	 *     freedom.  This is usually denoted P(chi**2 | nu).  This procedure
	 *     also computes Q(chi**2 | nu) = 1 - P(chi**2 | nu).  CHISQ and NU
	 *     must both be non-negative, and at most one of them may be zero.
	 * */
	/*--D replaces "?": ?CDCHI, ?gami
	 * */
	dgami( 0.5e0*nu, 0.5e0*chisq, p, q, ierr );
	return;
} /* end of function */
 
