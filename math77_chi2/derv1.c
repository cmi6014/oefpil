/*Translated by FOR_C, v3.4.2 (-), on 07/09/115 at 08:30:52 */
/*FOR_C Options SET: ftn=u io=c no=p op=aimnv s=dbov str=l x=f - prototypes */
#include <math.h>
#include "fcrt.h"
#include "derv1.h"
#include <stdio.h>
#include <stdlib.h>
	/* COMMON translations */
struct t_m77err {
	long int idelta, ialpha;
	}	m77err;
	/* end of COMMON translations */
void /*FUNCTION*/ derv1(
char *label,
double value,
byte flag)
{
 
	/* Copyright (c) 1996 California Institute of Technology, Pasadena, CA.
	 * ALL RIGHTS RESERVED.
	 * Based on Government Sponsored Research NAS7-03001.
	 *>> 1994-10-20 DERV1  Krogh  Changes to use M77CON
	 *>> 1994-04-20 DERV1  CLL Edited to make DP & SP files similar.
	 *>> 1985-09-20 DERV1  Lawson  Initial code.
	 *--D replaces "?": ?ERV1
	 *
	 *     ------------------------------------------------------------
	 *     SUBROUTINE ARGUMENTS
	 *     --------------------
	 *     LABEL     An identifing name to be printed with VALUE.
	 *
	 *     VALUE     A floating point number to be printed.
	 *
	 *     FLAG      See write up for FLAG in ERMSG.
	 *
	 *     ------------------------------------------------------------
	 * */
 
	if (m77err.ialpha >= -1)
	{
		printf("  %s  = %g \n", label, value);
		if (flag == '.')
			erfin();
	}
	return;
 
} /* end of function */
 
