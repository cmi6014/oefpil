#include <oefpil.h>
#include <tinyexpr.h>

#include "oefpil-fit.h" // real

typedef struct {
    int nv;
    int np;
    double *x;
    double *p;
    te_expr *expr_f;
    te_expr **exprs_fx;
    te_expr **exprs_fp;
} fcn_expr;

fcn_expr* fcn_expr_from_file(const char *fn);
int fcn_expr_minpack(void *data, int m, int n, const real *p, real *fvec, real *fjac, int ldfjac, int iflag);
int fcn_expr_oefpil(void *data, int n, const double *x, const double *p, double *fx, double *dfdx, double *dfdp);

void fcn_expr_free(fcn_expr *ef);

