#include <cminpack.h>

/* is it useful? */
/* typedef int fitfunc_minpack(void *p, int m, int n, const real *x, real *fvec, real *fjac, int ldfjac, int iflag); */

/* zkontrolovat delky wa, jestli nemaji vic zaviset na np / ndata */
void fit_minpack(void *fcn, void *data_fcn, int np, real *p, int ndata, real *x, real *y, int info)
{
    data_minpack data;

    real *fvec, *fjac, *diag, *qtf, *wa1, *wa2, *wa3, *wa4;
    int ldfjac;
    int *ipvt;
    int nfev, njev;
    int i;
    real fnorm;

    int maxfev, mode, nprint;
    real tol, ftol, xtol, gtol;

    printf("### CMINPACK begin ###\n\n");

    fvec = (real*)malloc(ndata * sizeof(real));
    fjac = (real*)malloc(ndata*np * sizeof(real));
    ldfjac = ndata;

    ipvt = (int*)malloc(np * sizeof(real));

    diag = (real*)malloc(np * sizeof(real));
    qtf = (real*)malloc(np * sizeof(real));
    wa1 = (real*)malloc(np * sizeof(real));
    wa2 = (real*)malloc(np * sizeof(real));
    wa3 = (real*)malloc(np * sizeof(real));
    wa4 = (real*)malloc(ndata*np * sizeof(real)); // nebo jenom ndata?

    data.x = x;
    data.y = y;
    data.fcndata = data_fcn;
    tol = 0.0001;

    /*
      info = __cminpack_func__(lmder1)(fcn, &data, ndata, np, p, fvec, fjac, ldfjac, tol, 
      ipvt, wa, lwa);
    */

    /* defaults as used by lmder1 */
    const real factor = 100.;
    maxfev = (np + 1) * 100;
    ftol = tol;
    xtol = tol;
    gtol = 0.;
    mode = 1;
    nprint = 1; /* lmder1: nprint = 0; */
    
    info = __cminpack_func__(lmder)(fcn, &data, ndata, np, p, fvec, fjac, ldfjac, ftol, xtol, gtol, 
				    maxfev, diag, mode, factor, nprint, &nfev, &njev, 
				    ipvt, qtf, wa1, wa2, wa3, wa4);

    fnorm = __cminpack_func__(enorm)(ndata, fvec);
    ftol = __cminpack_func__(dpmpar)(1);

    printf("\n");
    printf("final l2 norm of the residuals%15.7g\n", (real)fnorm);
    printf("number of function evaluations%15i\n", nfev);
    printf("number of Jacobian evaluations%15i\n", njev);
    printf("exit parameter                %15i\n", info);
    
    printf("final approximate solution\n");
    for (i = 0; i < np; ++i) {
	printf("%s% 15.6e", i%3==0?"\n     ":"", (real)p[i]);
    }

    printf("\n\n");
    printf("### CMINPACK end ###\n\n");

    free(fvec);
    free(fjac);
    free(ipvt);
    free(diag);
    free(qtf);
    free(wa1);
    free(wa2);
    free(wa3);
    free(wa4);
}
