#include "oefpil_excel_demo.h"

#include "..\\library\oefpil_excel.h"
#include <float.h> /* DBL_EPSILON */
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

void testdll()
{
    typedef double (*LPGETNUMBER)();
    typedef void (*LPVOID)();
    typedef void (*LPGETLONG)();
    double ddd[2];
    LPGETLONG lpbla4;
    HINSTANCE hModule = LoadLibrary(L"c:\\Software\\oefpil\\msvc2022\\oefpil\\x64\\Release\\oefpil.dll");

    wchar_t buf[256];
    FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        buf, (sizeof(buf) / sizeof(wchar_t)), NULL);

    //pbla1 = (double*)GetProcAddress(hModule, "bla1");
    //lpbla3 = (LPGETNUMBER)GetProcAddress((HMODULE)hModule, "bla3");

    //lpbla3(ddd);

    //lpbla4 = (LPGETNUMBER)GetProcAddress((HMODULE)hModule, "bla4");

    lpbla4 = (LPGETLONG)GetProcAddress((HMODULE)hModule, "bla4");

    lpbla4(ddd);


    FreeLibrary(hModule);
}


int main(int argc, char *argv[])
{   
    //////////////////////////////////////
    typedef double (*LPGETNUMBER)();
    typedef void (*LPVOID)();
    typedef void (*LPGETLONG)();
    double ddd[2];
    LPGETLONG lpoefpilexcel;
    HINSTANCE hModule = LoadLibrary(L"c:\\Software\\oefpil\\msvc2022\\oefpil\\x64\\Release\\oefpil.dll");

    wchar_t buf[256];
    FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        buf, (sizeof(buf) / sizeof(wchar_t)), NULL);


    lpoefpilexcel = (LPGETLONG)GetProcAddress((HMODULE)hModule, "oefpilexcel");
    ///////////////////////////////////////

    int i;

    int ndata; /*vstup*/
    int np = 2; /*fix */
    double *x, *y;  /* vstup*/
    double *ux, *uy;/* vstup*/
    double *params; /*vystup */
    double *pcov; /*vystup */
    double *xtrue, *ytrue; /*vystup */


    /* OEFPIL */

    /*priklad vstupnich hodnot */
   ndata = 10; // pocet dat
   x = (double *)malloc(ndata*sizeof(double));  /* TOHLE SE BUDE BRAT Z SLOUPECKU V EXCELU */ 
   y = (double *)malloc(ndata*sizeof(double)); /* TOHLE SE BUDE BRAT Z SLOUPECKU V EXCELU */

   x[0] =  0.0; y [0] = 5.9;
   x[1] =  0.9; y [1] = 5.4;
   x[2] =  1.8; y [2] = 4.4;
   x[3] =  2.6; y [3] = 4.6;
   x[4] =  3.3; y [4] = 3.5;
   x[5] =  4.4; y [5] = 3.7;
   x[6] =  5.2; y [6] = 2.8;
   x[7] =  6.1; y [7] = 2.8;
   x[8] =  6.5; y [8] = 2.4;
   x[9] =  7.4; y [9] = 1.5;

   /* nejistoty */
   ux = (double *)malloc(ndata*sizeof(double)); /* TOHLE SE BUDE BRAT Z SLOUPECKU V EXCELU */
   uy = (double *)malloc(ndata*sizeof(double)); /* TOHLE SE BUDE BRAT Z SLOUPECKU V EXCELU */

   for (i=0;i< ndata; i++){
	   ux[i]=1.0;
	   uy[i]=1.0;
   }

   /* primka 2 parametry  */
   np =2 ;
   /*vstup i vystup */
   params = (double*)malloc(np * sizeof(double));
   /*vystup */
   pcov = (double*)malloc(np*np * sizeof(double));

   /*vystup */
    xtrue = (double*)malloc(ndata * sizeof(double));
    ytrue = (double*)malloc(ndata * sizeof(double));

   //oefpilexcel(ndata, x, y, ux, uy, xtrue, ytrue, np, params, pcov);

   lpoefpilexcel(ndata, x, y, ux, uy, xtrue, ytrue, np, params, pcov);
   FreeLibrary(hModule);

   printf("slope %g \n", params[0]); /* VYSTUP FUNKCE REGUNC_SLOPE */
   printf("intercept %g \n", params[1]); /* VYSTUP FUNKCE REGUNC_INTERCEPT */
   printf("uncertainty-slope %g \n", sqrt(pcov[0])); /* VYSTUP FUNKCE REGUNC_UNCSLOPE */
   printf("uncertainty-intercept %g \n", sqrt(pcov[3])); /* VYSTUP FUNKCE REGUNC_UNCINTERCEPT */
   printf("correlation slope-intercept %g \n", pcov[1]/sqrt(pcov[0]*pcov[3])); /* VYSTUP FUNKCE REGUNC_CORRSLOPEINTERCEPT */

    free(params);
    free(pcov);

    free(x);
    free(y);
    free(xtrue);
    free(ytrue);
    free(ux);
    free(uy);

    Sleep(5000);

}
