#include "oefpil-fit.h"

#include "fcn-expr.h"

#include "oefpil.h"
#include "blaslapack.h"

/* #include <minpack.h> */
/* for e.g. minpack_lmder() */

#include <float.h> /* DBL_EPSILON */
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>


/* typedef struct { */
/*     double *x; */
/*     double *y; */
/*     /\* double *wght; *\/ */
/*     void* fcndata; */
/* } data_minpack; */

/* typedef double real; // should be somewhere in cminpack? */


static int numlines(const char *fn)
{
    FILE *f;
    int c;
    int nl;

    f = fopen(fn, "r");

    nl = 0;
    
    if (f) {
        for (c = getc(f); c != EOF; c = getc(f))
            if (c == '\n')
                nl++;

        fclose(f);
    }
    else {
        printf("Could not open file %s, exiting." "\n", fn);
        exit(1);
    }

    return nl;
}

static void get_dims(const char *fn, int *m, int *n)
{
    FILE *f;
    int c;
    int nc, nl;
    bool in_number;

    *m = 0;
    *n = 0;

    nl = 0;
    nc = 0;

    f = fopen(fn, "r");

    if (f) {
        for (c = getc(f); c != EOF; c = getc(f))
            if (c == '\n')
                nl++;

        if (nl > 0) {
            rewind(f);
            in_number = false;

            for (c = getc(f); c != '\n'; c = getc(f)) {
                if (!in_number && ((c != ' ') && (c != '\t'))) {
                    nc++;
                    in_number = true;
                }
                else if (in_number && ((c == ' ') || (c == '\t'))) {
                    in_number = false;
                }
            }
        }
        
        fclose(f);
    }
    else {
        printf("Could not open file %s, exiting." "\n", fn);
        exit(1);
    }

    *m = nl;
    *n = nc;
}

static void readmatrix(const char *fn, int m, int n, double *a)
{
    int i, j;
    FILE *f;

    f = fopen(fn, "r");

    if (f) {
        for (i = 0; i < m; i++) {
            for (j = 0; j < n; j++)
                fscanf(f, "%lf" , &(a[j*m + i]));
        }

        fclose(f);
    }
    else {
        printf("Could not open file %s, exiting." "\n", fn);
        exit(1);
    }
}

static void cov2unccorr(int n, const double *a, double *u, double *corr)
{
    int i, j;

    /* not elegant */
    
    for (i = 0; i < n; i++)
        u[i] = sqrt(a[i*n + i]);

    for (j = 0; j < n; j++)
        for (i = 0; i < n; i++)
            corr[j*n + i] = a[j*n + i] / (u[i] * u[j]);
}

static void printhelp()
{
    printf("oefpil-fit -f functionfile -m E/I -d datafile -x xcovfile -y ycovfile -c xycovfile" " \n");
    printf("           -p initparamfile -v verbosity -e truefile -t tolerance -n maxit -r convcrit [-s]" "\n");
    printf("\n");
    printf("              functionfile: function specification in JSON format, see examples" "\n");
    printf("              datafile: values in columns for individual variables" "\n");
    printf("              x and y-covariance files: either full matrix, or simplified - one variance at a line for each variable" "\n");
    printf("              initial parameter values: one value at a line for each parameter" "\n");
    printf("              optional arguments" "\n");
    printf("              xycovfile:  full matrix, default zero matrix" "\n");
    printf("              verbosity: 0 - silent" "\n");
    printf("                         1 - iteration progress for parameter values estimates" "\n");
    printf("                         2 - iteration progress for parameter values and covariance estimates" "\n");
    printf("                         2 - iteration progress for parameter values and covariance estimates" "\n");
    printf("                         default 0 " "\n");
    printf("              x and y true values file: initial estimate of the true values of x and y" "\n");
    printf("              tolerance: convergence tolerance, default exp(log(DBL_EPSILON)*2.0/3) " "\n");
    printf("              convergence criterion:  " "\n");
    printf("                                     0 - convergence in relative change of parameter values" "\n");
    printf("                                     1 - convergence in relative change of parameter values or " "\n");
    printf("                                         convergence of absolute values of parameters" "\n");
    printf("                                     2 - convergence in relative change of true xy values" "\n");
    printf("                                     3 - convergence in relative change of true xy values or " "\n");
    printf("                                         convergence of absolute values of true xy values" "\n");
    printf("                                     4 - convergence in relative change of parameter values and" "\n");
    printf("                                         convergence in relative change of true xy values" "\n");
    printf("                                     5 - (convergence in relative change of parameter values or " "\n");
    printf("                                         convergence of absolute values of parameters) and " "\n");
    printf("                                         (convergence in relative change of parameter values or " "\n");
    printf("                                         convergence of absolute values of parameters)" "\n");
    printf("                                     6 - convergence in chi2" "\n");
    printf("                                     default 5 " "\n");
    printf("              -s: rescale the resulting covariance matrix using the sigma2 factor" "\n");
    printf("\n");

    printf("See the examples directory how the input data should look like" "\n");
    printf("\n");
}

double generateGaussian() {
    static double spare;
    static bool hasSpare = false;
        double u, v, s;

    if (hasSpare) {
        hasSpare = false;
        return spare ;
    } 
    else {
        do {
            u = (rand() / ((double)RAND_MAX)) * 2.0 - 1.0;
            v = (rand() / ((double)RAND_MAX)) * 2.0 - 1.0;
            s = u * u + v * v;
        } while (s >= 1.0 || s == 0.0);

        s = sqrt(-2.0 * log(s) / s);
        spare = v * s;
        hasSpare = true;

        return u * s;
    }
}

void generate_Gaussian_noise_couple(double *u, double *v) {
    double s;

    do {
        *u = (rand() / ((double)RAND_MAX)) * 2.0 - 1.0;
        *v = (rand() / ((double)RAND_MAX)) * 2.0 - 1.0;
        s = (*u) * (*u) + (*v) * (*v);
    } while (s >= 1.0 || s == 0.0);

    s = sqrt(-2.0 * log(s) / s);
    *u *= s;
    *v *= s;
}

void generate_data(const int ndata, const double *x, const double *y, double *xMC, double *yMC, double *amat, int tilingmode)
{


    int i,n;
    double *z, u,v;
    
    n = 2*ndata;
    //TODO FUNCTION_IMPLICIT
    switch (tilingmode){
        case TILING_DIAG:
        case TILING_BLOCKDIAG:
        case TILING_DIAGS:
        case TILING_FULL:
            z = (double *)malloc(n*sizeof(double));
            
            for (i = 0; i < ndata; i++){
                   generate_Gaussian_noise_couple(&u, &v);
                   z[i] = u;
                   z[i+ndata] = v;
            }

            
            dtrmv("L", "N", "N", n, amat, n, z, 1);

            for (i = 0; i< ndata; i++){
                xMC[i] = x[i]+z[i];
                yMC[i] = y[i]+z[i+ndata];
            }
//            printmatrix(stdout, ndata, 1, xMC, PRINT_FORMAT_E8, "#xMC", 0);
 //           printmatrix(stdout, ndata, 1, yMC, PRINT_FORMAT_E8, "#yMC", 0);
            free(z);
    }
}
int main(int argc, char *argv[])
{
    int c;
    int i, ii, ndata, np, nv, nve, info, km, kn;
    int j,k, lpkinfo;
    int niter, maxit;
    double tol;
    double chi2;
    double sigma2, pval;
    bool usesigma2=false;

    double *pcov, *punc, *pcorr;

    /* bool prefit_minpack; */
    /* void *fcn_minpack; */
    void *fcn_oefpil;
    void *data_fcn;
    int fun_mode;
    double *params;
    double *x, *y, *K, *Kxx, *Kyy, *Kxy = NULL;
    double *xtrue, *ytrue;
    int tilingmode, tmx, tmy, tmxy;
    int *tilemap;

    char *funfn = NULL, *funmodestr =NULL, *datafn = NULL; 
    char *xcovfn = NULL, *ycovfn =NULL, *xycovfn =NULL, *parfn=NULL, *truefn=NULL;
    
    int printlevel , conv_crit ;

    fcn_expr *exprfcn;

    FILE *Aiter, *Apcov;
    FILE *Apar;
    FILE *Ax, *Ay;

    int iMC;
    int num_NAN_INF = 0, num_LAPACK = 0,num_UNKNOWN = 0, nMC=0, numMC = 0;
    double *avgp, *stdp, *covp, *corp;
    double *uncp; 
    double *xtrueMC;
    double *xMC, *yMC = NULL;
    double *parMC, *pcovMC;
    int  *infoMC, *niterMC;
    char fnmMCiter[200], fnmMCpar[200], fnmMCpcov[200], *fnmMC = NULL;
    char fnmMCx[200], fnmMCy[200];
    double *amat;
    double *mcpar, *mcpcov, *mcx, *mcy, *mcxtrue, *mcytrue;


    /* const char *argp_program_version = "devel"; */
    /* const char *argp_program_bug_address = "<rslesinger@cmi.cz>"; */

    /* argp_parse(0, argc, argv, 0, 0, 0); */

    /* prefit_minpack = false; */

    if (argc == 1){
        printhelp();
        exit(0);
    }

    printlevel = 0;
    maxit = 100; /* this really should suffice */
    tol = exp(log(DBL_EPSILON)  * 2.0/3); /* DBL_EPSILON^2/3 as used in ODRPACK; likely unreasonably strict */
    conv_crit = REL_PAR_OR_ABS_PAR_AND_REL_X_OR_ABS_X;

    while ((c = getopt(argc, argv, "f:m:d:x:y:c:p:e:v:t:n:r:hM:s")) != -1){
        switch(c){
            case 'f':
                funfn = optarg;
                break;
            case 'm':
                funmodestr = optarg;
                break;
            case 'd':
                datafn = optarg;
                break;
            case 'x':
                xcovfn = optarg;
                break;
            case 'y':
                ycovfn = optarg;
                break;
            case 'c':
                xycovfn = optarg;
                break;
            case 'p':
                parfn = optarg;
                break;
            case 'e':
                truefn = optarg;
                break;
            case 'v':
                printlevel = atoi(optarg);
                break;
            case 't':
                tol = strtod(optarg,NULL);
                break;
            case 'n':
                maxit = atoi(optarg);
                break;
            case 'r':
                conv_crit = atoi(optarg);
                break;
            case 'M':
                nMC = atoi(optarg);
                break;
            case 's':
                usesigma2 = true;
                break;
            case 'h':
            default:
                printhelp();
                exit(0);
                break;
        }
    }


    printf("\n");
    printf("******************" "\n");
    printf("***** OEFPIL *****" "\n");
    printf("******************" "\n\n");

    /* function */
    if (!funfn){
        printf("Need function file, exiting");
        exit(1);
    }
    if (!strcmp(funfn, "help")) { /* do not call your function file "help" */
	printhelp();
	exit(0);
    }
    
    exprfcn = fcn_expr_from_file(funfn);
    data_fcn = exprfcn;
    /* fcn_minpack = fcn_expr_minpack; */
    fcn_oefpil = fcn_expr_oefpil;
    nv = exprfcn->nv;
    np = exprfcn->np;

    pcov = (double*)malloc(np*np * sizeof(double));
    punc = (double*)malloc(np * sizeof(double));
    pcorr = (double*)malloc(np*np * sizeof(double));

    /*  mode */
    if (!funmodestr){
        printf("Need function mode, exiting. \n");
        exit(1);
    }
    switch (funmodestr[0]) {
    case 'e':
    case 'E':
        fun_mode = FUNCTION_EXPLICIT;
	nve = nv + 1;
        break;

    case 'i':
    case 'I':
        fun_mode = FUNCTION_IMPLICIT;
	nve = nv;
        break;

    default:
        printf("Unknown function mode %s" "\n", funmodestr);
        exit(0);
        break;
    }
    printf("nve %d \n", nve);

    if (!datafn){
        printf("Need input data, exiting \n");
    }

    /*  data */
    ndata = numlines(datafn);

    x = (double*)malloc(nve*ndata * sizeof(double));
    readmatrix(datafn, ndata, nve, x);

    if (fun_mode == FUNCTION_EXPLICIT){
        y = x + nv*ndata;
    }

    /*  x covariance */
    if (xcovfn) {
        km = 0;
        kn = 0;
        get_dims(xcovfn, &km, &kn);

        tmx = TILE_NULL;
        printf("km %d kn %d tmx %d \n", km ,kn, tmx);
        printf("nv*ndata %d \n", nv*ndata);
    
        if ((km == 1) && (kn == nv)) { /* simple */
            Kxx = (double*)malloc(nv*ndata * sizeof(double));
            readmatrix(xcovfn, nv, 1, Kxx);

            for (ii = 0; ii < nv; ii++) {
                for (i = 1; i < ndata; i++)
                    Kxx[ii*ndata + i] = Kxx[ii*ndata];
            }
            tmx = TILE_DIAG;
        }
        else if ((km == ndata) && (kn == 1)) { /* diagonal */
            Kxx = (double*)malloc(ndata * sizeof(double));
            readmatrix(xcovfn, ndata, 1, Kxx);
            tmx = TILE_DIAG;
        }
        else if ((km == nv*ndata) && (kn == nv*ndata)) { /* full */
            Kxx = (double*)malloc(nv*ndata * nv*ndata * sizeof(double));
            readmatrix(xcovfn, nv*ndata, nv*ndata, Kxx);
            tmx = TILE_FULL;
        }
        else if ((km == ndata) && (kn == ndata) && (fun_mode == FUNCTION_IMPLICIT)){
            Kxx = (double*)malloc(ndata*ndata*sizeof(double));
            readmatrix(xcovfn, ndata,ndata,Kxx);
            tmx = TILE_FULL;
        }
        else { /* wrong */
        }
    }
    //printf("tmx po %d \n", tmx);
    //printf("nv %d ndata %d \n", nv, ndata);
   // printmatrix(stdout, nv*ndata, nv*ndata, Kxx, PRINT_FORMAT_E8, "Kxx", 0);

    /*  y covariance */
    tmy = TILE_NULL;

    if (ycovfn) {
        km = 0;
        kn = 0;
        get_dims(ycovfn, &km, &kn);

        tmy = TILE_NULL;
        printf("km %d kn %d tmy %d \n", km ,kn, tmy);

        if ((km == 1) && (kn == 1)) { /* simple -> diagonal */
            Kyy = (double*)malloc(ndata * sizeof(double));
            readmatrix(ycovfn, 1, 1, Kyy);

            for (i = 1; i < ndata; i++)
                Kyy[i] = Kyy[0]; // we assume there are more data points than variables, quite naturally

            tmy = TILE_DIAG;
        }
        else if ((km == ndata) && (kn == 1)) { /* diagonal */
            Kyy = (double*)malloc(ndata * sizeof(double));
            readmatrix(ycovfn, ndata, 1, Kyy);
            tmy = TILE_DIAG;
        }
        else if ((km == ndata) && (kn == ndata)) { /* full */
            Kyy = (double*)malloc(ndata * ndata * sizeof(double));
            readmatrix(ycovfn, ndata, ndata, Kyy);
            tmy = TILE_FULL;
        }
        else { /* wrong */
        }
    }
    /*printf("tmy po %d \n", tmy);
    printf("nv %d ndata %d \n", nv, ndata);
    printmatrix(stdout, nv*ndata, nv*ndata, Kyy, PRINT_FORMAT_E8, "Kyy", 0);
    */
        
    /*  x-y covariance */
    tmxy = TILE_NULL;

    if (xycovfn) {
	km = 0;
	kn = 0;
	get_dims(xycovfn, &km, &kn);

        printf("km %d kn %d tmxy %d \n", km ,kn, tmxy);


	if ((km == ndata) && (kn == 1)) { /* diagonal */
	    Kxy = (double*)malloc(ndata * sizeof(double));
	    readmatrix(xycovfn, ndata, 1, Kxy);
	    tmxy = TILE_DIAG;
	}
	else if ((km == ndata) && (kn == ndata)) { /* full */
	    Kxy = (double*)malloc(ndata * ndata * sizeof(double));
	    readmatrix(xycovfn, ndata, ndata, Kxy);
	    tmxy = TILE_FULL;
	}
    }
    printf("tmx %d tmy %d tmxy %d \n", tmx, tmy,tmxy);
    /*
    printf("tmxy po %d \n", tmxy);
    printf("nv %d ndata %d \n", nv, ndata);
    printmatrix(stdout, nv*ndata, nv*ndata, Kxy, PRINT_FORMAT_E8, "Kxy", 0);
    */

    /* initial parameter values */
    if (!parfn){
        printf("Need initial parameter values, exiting \n"); 
        exit(1);
    }
    params = (double*)malloc(np * sizeof(double));
    printf("Reading initial parameter values from %s" "\n", parfn);
    readmatrix(parfn, np, 1, params);

    /* initial xtrue,ytrue values */
    if (truefn){
        printf("Reading initial true values from %s" "\n", truefn);
        xtrue = (double*)malloc(nve*ndata * sizeof(double));
        readmatrix(truefn, ndata, nve, xtrue);
        if (fun_mode == FUNCTION_EXPLICIT)
            ytrue = xtrue + nv*ndata;
    }
    else{
        xtrue = (double*)malloc(nve*ndata * sizeof(double));
        for (i = 0; i < ndata*nve; i++)
            xtrue[i] = x[i];
        if (fun_mode == FUNCTION_EXPLICIT)
                ytrue = xtrue + nv*ndata ;
    }



    
    /* only for nve = 2 */

    if ((tmx == TILE_DIAG) && (tmy == TILE_DIAG))
        switch (tmxy) {
            
        case TILE_NULL:
            tilingmode = TILING_DIAG;
            tilemap = oefpil_tilemap_diagtiles_new(nve);
            K = oefpil_tcm_diag_new(ndata, nve, tilemap);

            oefpil_tcm_diag_set_tile_diag(ndata, nve, K, tilemap, 0, Kxx);
            oefpil_tcm_diag_set_tile_diag(ndata, nve, K, tilemap, 1, Kyy);
            break;
            
        case TILE_DIAG:
            tilingmode = TILING_DIAGS;
            tilemap = oefpil_tilemap_alltiles_new(nve);
            K = oefpil_tcm_diags_new(ndata, nve, tilemap);

            oefpil_tcm_diags_set_tile_diag(ndata, nve, K, tilemap, 0, 0, Kxx);
            oefpil_tcm_diags_set_tile_diag(ndata, nve, K, tilemap, 1, 1, Kyy);
            oefpil_tcm_diags_set_tile_diag(ndata, nve, K, tilemap, 0, 1, Kxy); /* takes care of 1,0 */
            break;
            
        case TILE_FULL:
            tilingmode = TILING_FULL;
            tilemap = oefpil_tilemap_alltiles_new(nve);
            K = oefpil_tcm_full_new(ndata, nve, tilemap);

            oefpil_tcm_full_set_tile_diag(ndata, nve, K, tilemap, 0, 0, Kxx);
            oefpil_tcm_full_set_tile_diag(ndata, nve, K, tilemap, 1, 1, Kxy);
            oefpil_tcm_full_set_tile_full(ndata, nve, K, tilemap, 0, 1, Kxy); /* takes care of 1,0 */
            break;
        }
    else if ((tmx == TILE_FULL) && (tmy == TILE_NULL) && (tmxy == TILE_NULL)){
        if (fun_mode == FUNCTION_EXPLICIT)
            printf("Warning! This should be used only for implicit functions.\n");

                tilingmode = TILING_FULL;
                tilemap = oefpil_tilemap_alltiles_new(nve);
                K = oefpil_tcm_full_new(ndata, nve, tilemap);
 //               oefpil_tcm_full_set_tile_full(ndata, nve, K, tilemap, 0, 0, Kxx);
                for (i = 0;i < nve*nve; i++)
                    tilemap[i] = TILE_FULL;
                K = Kxx;
    }
    else if ((tmx == TILE_FULL) || (tmy == TILE_FULL)){
        switch (tmxy) {
            
        case TILE_NULL:
            tilingmode = TILING_BLOCKDIAG;
            tilemap = oefpil_tilemap_diagtiles_new(nve);
            K = oefpil_tcm_blockdiag_new(ndata, nve, tilemap);
            
            if (tmx == TILE_DIAG)
                oefpil_tcm_blockdiag_set_tile_diag(ndata, nve, K, tilemap, 0, Kxx);
            else
                oefpil_tcm_blockdiag_set_tile_full(ndata, nve, K, tilemap, 0, Kxx);
            
            if (tmy == TILE_DIAG)
                oefpil_tcm_blockdiag_set_tile_diag(ndata, nve, K, tilemap, 1, Kyy);
            else
                oefpil_tcm_blockdiag_set_tile_full(ndata, nve, K, tilemap, 1, Kyy);
            
            break;

        case TILE_DIAG:
        case TILE_FULL:
            tilingmode = TILING_FULL;
            tilemap = oefpil_tilemap_alltiles_new(nve);
            K = oefpil_tcm_full_new(ndata, nve, tilemap);

            if (tmx == TILE_DIAG)
                oefpil_tcm_full_set_tile_diag(ndata, nve, K, tilemap, 0, 0, Kxx);
            else
                oefpil_tcm_full_set_tile_full(ndata, nve, K, tilemap, 0, 0, Kxx);

    printmatrix(stdout, nve*ndata, nve*ndata, K, PRINT_FORMAT_E8, "Cov tmx", 0);

            if (tmy == TILE_DIAG)
                oefpil_tcm_full_set_tile_diag(ndata, nve, K, tilemap, 1, 1, Kyy);
            else
                oefpil_tcm_full_set_tile_full(ndata, nve, K, tilemap, 1, 1, Kyy);
    printmatrix(stdout, nve*ndata, nve*ndata, K, PRINT_FORMAT_E8, "Cov tmy", 0);

            if (tmxy == TILE_DIAG)
                oefpil_tcm_full_set_tile_diag(ndata, nve, K, tilemap, 0, 1, Kxy); /* takes care of 1,0 */
            else{
                oefpil_tcm_full_set_tile_full(ndata, nve, K, tilemap, 0, 1, Kxy); /* takes care of 1,0 */
            }

    printmatrix(stdout, nve*ndata, nve*ndata, K, PRINT_FORMAT_E8, "Cov tmxy", 0);

            break;
        }
    }
    else if (nve > 2) {
        tilingmode = TILING_DIAG;
        tilemap = oefpil_tilemap_diagtiles_new(nve);
        K = oefpil_tcm_diag_new(ndata, nve, tilemap);

        /* set first block */
        for (i = 0; i < ndata; i++)
            K[i] = 1.0;

        /* copy to other blocks */
        for (i = 1; i < nve; i++)
            oefpil_tcm_diag_set_tile_diag(ndata, nve, K, tilemap, i, K);
    }
    else {
        printf("Cannot assemble the tiled covariance matrix, exiting." "\n");
        exit(1);
    }
	       
  //  printf("K tiling mode: %d" "\n", tilingmode);
//    printmatrix(stdout, nve*ndata, nve*ndata, K, PRINT_FORMAT_E8, "Cov", 0);

    printf("Parameters of iterations: \n");
    printf("    tolerance %g \n", tol);
    printf("    maximum iterations %d \n", maxit);
    printf("    convergence criterion ");
    switch(conv_crit){
        case 0: 
            printf("   convergence in relative change of parameter values" "\n");
            break;
        case 1:
            printf("   convergence in relative change of parameter values or convergence of absolute values of parameters" "\n");
            break;
        case 2:
            printf("   convergence in relative change of true xy values" "\n");
            break;
        case 3:
            printf("   convergence in relative change of true xy values or convergence of absolute values of true xy values" "\n");
            break;
        case 4:
            printf("   convergence in relative change of parameter values and convergence in relative change of true xy values" "\n");
            break;
        case 5:
            printf("   convergence in relative change of parameter values or convergence of absolute values of parameters) and " "\n");
            printf("    convergence in relative change of parameter values or convergence of absolute values of parameters)" "\n");
            break;
        case 6:
            printf("   convergence in chi2" "\n");
            break;
        default:
            printf("Unknown convergence criterion. Exiting.\n");
            exit(1);
            break;
    }
 
    /* OEFPIL */
    
    if (usesigma2)
        oefpil(fcn_oefpil, data_fcn, fun_mode, np, params, pcov, ndata, nv, x, y, xtrue, ytrue, K, tilingmode, tilemap, maxit, tol,
           printlevel, stdout, &chi2, conv_crit, &info, &niter, &sigma2, true, &pval);
    else
        oefpil(fcn_oefpil, data_fcn, fun_mode, np, params, pcov, ndata, nv, x, y, xtrue, ytrue, K, tilingmode, tilemap, maxit, tol,
           printlevel, stdout, &chi2, conv_crit, &info, &niter, &sigma2, false, &pval);

    switch (info) {

    case OEFPIL_ERROR_NAN:
    case OEFPIL_ERROR_INF:
        printf("\n");
        printf("******************************************************************************************" "\n");
        printf("*** ATTENTION! OEFPIL encountered a NaN or Inf value, the results should be discarded. ***" "\n");
        printf("******************************************************************************************" "\n");
        exit(1);
        break;
        
    case OEFPIL_ERROR_LAPACK:
        printf("\n");
        printf("************************************************************************************" "\n");
        printf("*** ATTENTION! OEFPIL encountered LAPACK ERROR, the results should be discarded. ***" "\n");
        printf("************************************************************************************" "\n");
        exit(1);
        break;

    case OEFPIL_MAX_ITER_REACHED:
    case OEFPIL_STATUS_UNKNOWN:
        printf("\n");
        printf("********************************************************************************************************" "\n");
        printf("*** ATTENTION! OEFPIL dit not converge successfully, the results below relate to the last iteration. ***" "\n");
        printf("********************************************************************************************************" "\n");
        break;

    default:
        printf("\n");
        printf("Summary" "\n");
        printf("-------" "\n");
        printf("\n");
    
        printf("Termination info: %d" "\n", info);
        printf("Number of iterations: %d" "\n", niter);
        printf("Sum of residuals: %.12g \n", chi2);
	printf("Factor between covariance and uncertainty: %.12g \n", sigma2);
	if (pval)
		printf("Chi2 pval: %.12g \n", pval);

        printf("\n");

        cov2unccorr(np, pcov, punc, pcorr);

        printmatrix(stdout, 1, np, params, PRINT_FORMAT_E8, "Parameter estimates", 0);
        printf("\n");

        printmatrix(stdout, np, np, pcov, PRINT_FORMAT_E8, "Parameter covariance matrix", 0);
        printf("\n");

        printmatrix(stdout, 1, np, punc, PRINT_FORMAT_E8, "Parameter uncertainties", 0); 
        printf("\n");

        printmatrix(stdout, np, np, pcorr, PRINT_FORMAT_F8, "Parameter correlation matrix", 0);
        printf("\n");

        if (printlevel > 2){
            printmatrix(stdout, ndata, nve, xtrue, PRINT_FORMAT_E8, "True values :", 0);
        }
        break;

    } /* end switch (info) */
  if (nMC){

            snprintf(fnmMCiter,200, "mciter");
            snprintf(fnmMCpar,200, "mcpar");
            snprintf(fnmMCpcov,200, "mcpcov");
            snprintf(fnmMCx,200, "mcx");
            snprintf(fnmMCy,200, "mcy");



       printf("################### Start Monte Carlo ########### \n");
       printf(" Number of iterations: %d \n", nMC);
       if (fnmMC) 
           printf(" Output prefix: %s \n", fnmMC);

       xMC = (double *)malloc(nve*ndata* nMC*sizeof(double));
       /*
       if (fun_mode == FUNCTION_EXPLICIT)
           yMC = xMC + nv*ndata;
           */

       printf("nMC %d \n", nMC);
       parMC = (double *) malloc(np*nMC*sizeof(double));
       pcovMC = (double *) malloc(np*np*nMC*sizeof(double));
       infoMC = (int *) malloc(nMC*sizeof(int));
       niterMC = (int *)malloc(nMC*sizeof(int));
       avgp = (double *) malloc(np*sizeof(double));
       stdp = (double *) malloc(np*sizeof(double));
       uncp = (double *) malloc(np*sizeof(double));
       covp = (double *) malloc(np*np*sizeof(double));
       corp = (double *) malloc(np*np*sizeof(double));

       xtrueMC = (double *) malloc(nve*ndata *nMC* sizeof(double));

       

       // initialization
       for (iMC = 0; iMC< nMC; iMC++){
           for (j = 0 ; j< np;j++)
               parMC[iMC*np +j] = params[j];

           for (i = 0; i < ndata*nve; i++){
               xtrueMC[iMC*ndata*nve + i] = xtrue[i];
           }
           /*           if (fun_mode == FUNCTION_EXPLICIT){
                        ytrueMC = xtrueMC + nv*ndata ;
                        }
                        */
       }

            if (printlevel > 2){
                Aiter = fopen(fnmMCiter, "w");
                Apar  = fopen(fnmMCpar,"w");
                Apcov = fopen(fnmMCpcov,"w");
                Ax = fopen(fnmMCx, "w");
                Ay = fopen(fnmMCy, "w");
            }


            for (j=0; j < np; j++){
                avgp[j] = 0;
                stdp[j] = 0;
                uncp[j] = 0;
            }
            for (j=0 ; j< np*np; j++){
                covp[j] = 0;
                corp[j] = 0;
            }

	    amat = (double *)malloc(4*ndata*ndata*sizeof(double));
	    for (i = 0; i < 4*ndata*ndata;i++){
		    amat[i] = 0;
	    }
	    if (nve != 2){
		    printf("Error: Monte Carlo implemented only for nve = 2. \n");
	    }

	    switch (tmx){
		    case TILE_DIAG:
			    printf("tmx TILE_DIAG\n");
			    for (i=0;i < ndata; i++){
				    amat[i+2*ndata*i] = Kxx[i];
				    }
			    break;
		    case TILE_FULL:
			    printf("tmx TILE_FULL\n");
			    for (i=0;i < ndata; i++){
				    for (j=0;j < ndata; j++){
					    amat[i+2*ndata*j] = Kxx[i+ndata*j];
				    }
			    }
			    break;
	    }
	    switch (tmy){
		    case TILE_DIAG:
			    printf("tmy TILE_DIAG\n");
			    for (i=0;i < ndata; i++){
				    amat[ndata+i+2*ndata*(i+ndata)] = Kyy[i];
			    }
			    break;
		    case TILE_FULL:
			    printf("tmy TILE_FULL\n");
			    for (i=0;i < ndata; i++){
				    for (j=0;j < ndata; j++){
					    amat[ndata+i+2*ndata*(ndata+j)] = Kyy[i+ndata*j];
				    }
			    }
			    break;
	    }
	    switch (tmxy){
		    case TILE_DIAG:
			    printf("tmxy TILE_DIAG\n");
			    for (i=0;i < ndata; i++){
				    amat[ndata+i+2*ndata*i] = Kxy[i];
				    amat[i+2*ndata*(i+ndata)] = Kxy[i];
			    }
			    break;
		    case TILE_FULL:
			    printf("tmxy TILE_FULL\n");
			    for (i=0;i < ndata; i++){
				    for (j=0;j < ndata; j++){
					    amat[i+ndata + 2*ndata*j] = Kxy[i+ndata*j];
					    amat[i + 2*ndata*(j+ndata)] = Kxy[i+ndata*j];
				    }
			    }
			    break;
	    }

            if (printlevel > 2)
                printmatrix(stdout, 2*ndata, 2*ndata, amat, PRINT_FORMAT_E8, "amat", 0);

            dpotrf("L", 2*ndata, amat, 2*ndata, &lpkinfo);

            if (lpkinfo != 0) {
                printf("lpkinfo %d \n", lpkinfo);
                printf("Error in Cholesky decomposition. \n");
                return 1;
            }
            if (printlevel > 2)
                printmatrix(stdout, 2*ndata, 2*ndata, amat, PRINT_FORMAT_E8, "sqrt(U)", 0);

           srand(time(NULL));
           // srand(0);
            
	   for (iMC = 0 ; iMC< nMC; iMC++){

		   mcx = xMC+iMC*nve*ndata;
		   mcy = mcx + nv*ndata;
		   mcxtrue = xtrueMC+iMC*nve*ndata;
		   mcytrue = mcxtrue + nv*ndata;
		   mcpar = parMC+iMC*np;
		   mcpcov = pcovMC+iMC*np*np;

		   generate_data(ndata, xtrue,ytrue, mcx, mcy, amat, tilingmode);


		   if (printlevel > 2){
			   for (j=0;j<ndata;j++){
				   fprintf(Ax, PRINT_FORMAT_E8, mcx[j]);
				   fprintf(Ay, PRINT_FORMAT_E8, mcy[j]);
			   }
			   fprintf(Ax, "\n");
			   fprintf(Ay, "\n");
		   fflush(Ax);
		   fflush(Ay);
		   }


		   if (printlevel > 1)                printf(" Monte Carlo iteration %d \n", iMC);
		   //      printmatrix(stdout, nve*ndata, nve*ndata, K, PRINT_FORMAT_E8, "Cov before MC oefpil", 0);
		   oefpil(fcn_oefpil, data_fcn, fun_mode, np, mcpar, mcpcov, ndata, nv, mcx, mcy, mcxtrue, mcytrue, K, 
				   tilingmode, tilemap, maxit, tol, 0, stdout, &chi2, conv_crit, &info, &niter, NULL, false, NULL);



		   infoMC[iMC] = info;
		   niterMC[iMC] = niter;

		   switch(info){
			   case OEFPIL_ERROR_NAN:
			   case OEFPIL_ERROR_INF:
				   num_NAN_INF++;
				   break;
			   case OEFPIL_ERROR_LAPACK:
				   num_LAPACK++;
				   break;
			   case OEFPIL_MAX_ITER_REACHED:
			   case OEFPIL_STATUS_UNKNOWN:
				   num_UNKNOWN++;
				   break;
			   default:
				   for (j = 0; j< np; j++){
					   avgp[j] += mcpar[j];
					   stdp[j] += mcpar[j]*mcpar[j];
					   uncp[j] += mcpar[j]*mcpar[j];
				   }
				   for (j = 0; j < np; j++){
					   for (k = 0; k< np; k++)
						   covp[j*np +k ] += mcpar[j]*mcpar[k];
				   }
				   numMC ++;

				   if (printlevel > 2){
					   for ( j = 0; j < np; j++){
						   fprintf(Apar,"%20.15g ", mcpar[j]);
					   }
					   fprintf(Apar,"\n");
					   for ( j = 0; j < np*np; j++)
						   fprintf(Apcov, "%g ", mcpcov[j]);
					   fprintf(Apcov, "\n");
				   }
		   }

		   if (printlevel > 2){
			   fprintf(Aiter, "%d %d \n", niter, info);
		   }

	   }

            /*eval MC results*/
            if (num_NAN_INF){
                printf("\n");
                printf("*********************************************************************************************************" "\n");
                printf("*** ATTENTION! OEFPIL encountered %d  a NaN or Inf value, the results should be checked. ******" "\n", num_NAN_INF);
                printf("*********************************************************************************************************" "\n");
            }
            if (num_LAPACK){
                printf("\n");
                printf("****************************************************************************************************" "\n");
                printf("*** ATTENTION! OEFPIL encountered at least once a LAPACK ERROR, the results should be discarded. ***" "\n");
                printf("****************************************************************************************************" "\n");
            }
            if (num_UNKNOWN){
                printf("\n");
                printf("***********************************************************************" "\n");
                printf("*** ATTENTION! OEFPIL dit not converge successfully at least once . ***" "\n");
                printf(" didn't converge %d \n", num_UNKNOWN);
                printf("***********************************************************************" "\n");
            }

            /* calculate average and stdev */
            for (j=0;j<np;j++){
                avgp[j] /= numMC;
                stdp[j] /= numMC;
                stdp[j] -= avgp[j]*avgp[j];
                stdp[j] = sqrt(stdp[j]);
                uncp[j] /= numMC;
                uncp[j] -= params[j]*params[j];
                uncp[j] = sqrt(uncp[j]);
            }
            for (j=0; j < np; j++){
                for (k = 0; k < np; k++){
                    covp[j*np + k ] /= numMC;
                    covp[j*np + k ] -= avgp[j]*avgp[k];
                    corp[j*np + k ] =covp[j*np + k]/stdp[j]/stdp[k];
                }
            }

            printf("\n");
            printf("********************************************************************************************************" "\n");
            printf("****************MONTE CARLO results ********************************************************************" "\n");
            printmatrix(stdout, 1, np, avgp, PRINT_FORMAT_E8, "Average parameter estimates", 0);
            printmatrix(stdout, 1, np, stdp, PRINT_FORMAT_E8, "Standard deviation of parameter estimates", 0);
            printmatrix(stdout, np, np, covp, PRINT_FORMAT_E8, "Covariance matrix of parameter estimates", 0);
            printmatrix(stdout, np, np, corp, PRINT_FORMAT_E8, "Correlation matrix of parameter estimates", 0);
            printf("********************************************************************************************************" "\n");

            

            if (printlevel > 2){
                fclose(Aiter);
                fclose(Apar);
                fclose(Apcov);
                fclose(Ax);
                fclose(Ay);
            }

            free(amat);
            free(xMC);
            free(xtrueMC);
            if (fun_mode == FUNCTION_EXPLICIT)
                free(yMC);

            free(parMC);
            free(pcovMC);
            free(infoMC);
            free(niterMC);

            free(avgp);
            free(stdp);
            free(covp);
            free(corp);
            free(uncp);
            /*end Monte Carlo */
        }
    
    free(params);
    free(pcov);
    free(punc);
    free(pcorr);
    free(x);
    free(xtrue);
    free(Kxx);
    if (fun_mode == FUNCTION_EXPLICIT)
        free(Kyy);
    free(K);
    free(tilemap);
    if (Kxy)
        free(Kxy);

    fcn_expr_free(exprfcn);
    
}
