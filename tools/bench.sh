#!/bin/bash
OPENBLAS_NUM_THREADS=1 nice -n -20 ./oefpil-bench 150000 50 0
OPENBLAS_NUM_THREADS=1 nice -n -20 ./oefpil-bench 15000 250 0
OPENBLAS_NUM_THREADS=1 nice -n -20 ./oefpil-bench 650 1250 0
