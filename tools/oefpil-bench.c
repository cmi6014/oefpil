#include "oefpil.h"

#include <math.h>
#include <stdio.h>
#include <time.h>

/*
double fcn_op(double x, double alpha, double hp, double m)
{
    return alpha * pow(x - hp, m);
}
*/

/*
  F/a = (x - hp)^m;
  log(F/a) = m * log(x - hp);
  log(x - hp) = log(F/a) / m;
  x - hp = exp(log(F/a) / m);
  x = exp(log(F/a) / m) + hp;
*/

double fcn_op_inv(double x, double alpha, double hp, double m)
{
    return exp(log(x/alpha) / m) + hp;
}


static double fcn_op(double x, const double *p)
{
    return p[0] * pow(x - p[1], p[2]);
}

static double d_fcn_op_dx(double x, const double *p)
{
    return p[0] * p[2] * pow(x - p[1], p[2] - 1);
}

static double d_fcn_op_dalpha(double x, const double *p)
{
    return pow(x - p[1], p[2]);
}

static double d_fcn_op_dhp(double x, const double *p)
{
    return -p[0] * p[2] * pow(x - p[1], p[2] - 1);
}

static double d_fcn_op_dm(double x, const double *p)
{
    return p[0] * pow(x - p[1], p[2]) * log(x - p[1]);
}

void fcn_op_oefpil(void *data, int n, const double *x, const double *p, double *wf, double *wfx, double *wfp)
{
    int i;

    for (i = 0; i < n; i++)
	wf[i] = fcn_op(x[i], p);

    for (i = 0; i < n; i++)
	wfx[i] = d_fcn_op_dx(x[i], p);

    for (i = 0; i < n; i++) {
	wfp[0*n + i] = d_fcn_op_dalpha(x[i], p);
	wfp[1*n + i] = d_fcn_op_dhp(x[i], p);
	wfp[2*n + i] = d_fcn_op_dm(x[i], p);
    }
}

/* taken from https://rosettacode.org/wiki/Statistics/Normal_distribution#C */
void nrand(double *a, double *b)
{
    double x, y, rsq, f;
    
    do {
	x = 2.0 * rand() / (double)RAND_MAX - 1.0;
	y = 2.0 * rand() / (double)RAND_MAX - 1.0;
	rsq = x*x + y*y;
    } while (rsq >= 1. || rsq == 0.);
    
    f = sqrt(-2.0 * log(rsq) / rsq);
    *a = x * f;
    *b = y * f;
}
    

void op_bench(int n, int ndata, int printlevel, double Fmax, double pctlo, double pcthi, double uh, double uF, double alpha, double hp, double m)
{
    double **x, **y;
    double *Kxx, *Kyy, *K, *pcov, *p;
    int *tm;
    double hlo, hhi, hres;
    double rx, ry;
    int i, j;
    int info, niter, maxit;
    double tol;
    int np;
    clock_t clk0, clk1;

    printf("### OEFPIL Oliver-Pharr benchmark ###\n");
    printf("# %d runs with %d points #\n", n, ndata);
    printf("# parameter values : %g %g %g #\n", alpha, hp, m);
    printf("  allocating large arrays ... ");
    fflush(stdout);
    
    clk0 = clock();
    if (clk0 == -1)
	printf("clock0 error!\n");

    x = (double**)malloc(n * sizeof(double*));
    y = (double**)malloc(n * sizeof(double*));

    for (i = 0; i < n; i++) {
	x[i] = (double*)malloc(ndata * sizeof(double));
	y[i] = (double*)malloc(ndata * sizeof(double));
    }

    Kxx = (double*)malloc(ndata * sizeof(double));
    Kyy = (double*)malloc(ndata * sizeof(double));
    
    clk1 = clock();
    if (clk1 == -1)
	printf("clock1 error!\n");
    
    printf(" done (%g s)\n", (clk1 - clk0)/(double)CLOCKS_PER_SEC);

    np = 3;
    
    p = (double*)malloc(np * sizeof(double));
    pcov = (double*)malloc(np*np * sizeof(double));

    hhi = fcn_op_inv(0.01 * pcthi * Fmax, alpha, hp, m);
    hlo = fcn_op_inv(0.01 * pctlo * Fmax, alpha, hp, m);
    hres = (hhi - hlo) / (ndata - 1);

    printf("  generating data ... ");
    fflush(stdout);

    p[0] = alpha;
    p[1] = hp;
    p[2] = m;

    // srand(time(0));
    // do not init, in order to have repeatable values

    for (i = 0; i < n; i++) {
	for (j = 0; j < ndata; j++) {
	    nrand(&rx, &ry);
	    x[i][j] = hlo + j*hres + rx*uh;
	    y[i][j] = fcn_op(hlo + j*hres, p) + ry*uF;
	}
    }

    tm = oefpil_tilemap_diagtiles_new(2);
    K = oefpil_tcm_diag_new(ndata, 2, tm);

    for (i = 0; i < ndata; i++) {
	Kxx[i] = uh*uh;
	Kyy[i] = uF*uF;
    }

    oefpil_tcm_diag_set_tile_diag(ndata, 2, K, tm, 0, Kxx);
    oefpil_tcm_diag_set_tile_diag(ndata, 2, K, tm, 1, Kyy);
	
    tol = 1e-06; /* approx. DBL_EPSILON^2/3 = 2e-11 */

    printf("done\n");
    printf("  starting calculation ...\n");
    fflush(stdout);
    
    clk0 = clock();
    if (clk0 == -1)
	printf("clock0 error!\n");

    for (i = 0; i < n; i++) {
	p[0] = alpha;
	p[1] = hp;
	p[2] = m;

	maxit = 100;
    
	oefpil(fcn_op_oefpil, NULL, FUNCTION_EXPLICIT,
	       3, p, pcov,
	       ndata, 1, x[i], y[i],
           NULL, NULL,
	       K, TILING_DIAG, tm,
	       maxit, tol,
	       printlevel, stdout,
           NULL, 0,
	       &info, &niter, NULL,false, NULL);

	/*
	switch (info) {
	case 1:
	    printf(".");
	    break;
	case 2:
	    printf("!");
	    break;
	case 3:
	    printf("X");
	    break;
	}
	fflush(stdout);
	*/ 
	/* printf("%g %g %g\t(%d iterations)\n", p[0], p[1], p[2], niter); */
	/* printf("%d: %g %g %g (%g %g %g), %d iterations\n", info, p[0], p[1], p[2], sqrt(pcov[0]), sqrt(pcov[3]), sqrt(pcov[6]), niter); */
    }

    /* printf("\n"); */

    clk1 = clock();
    if (clk1 == -1)
	printf("clock1 error!\n");

    printf(" ... done (%g s)\n", (clk1 - clk0)/(double)CLOCKS_PER_SEC);

    for (i = 0; i < n; i++) {
	free(x[i]);
	free(y[i]);
    }
    
    free(x);
    free(y);
}

int main(int argc, char *argv[])
{
    int n, ndata, printlevel;
    
    const double alpha = 0.01;
    const double hp = 500;
    const double m = 1.35;

    const double Fmax = 10.0;
    const double pctlo = 20.0;
    const double pcthi = 98.0;
    const double uh = 0.5;
    const double uF = 0.001;

    n = atoi(argv[1]);
    ndata = atoi(argv[2]);
    printlevel = atoi(argv[3]);

    op_bench(n, ndata, printlevel, Fmax, pctlo, pcthi, uh, uF, alpha, hp, m);
}
