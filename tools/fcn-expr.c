#include "fcn-expr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <jansson.h>

static void check_err(int err);

double pcws(double x, double a, double fx, double gx)
{
    if (x < a)
        return fx;
    else
        return gx;
}

static void check_err(int err)
{
    if (err == 0)
	printf("OK" "\n");
    else
	printf("error at position %d" "\n", err);
}

fcn_expr* fcn_expr_from_file(const char *fn)
{
    fcn_expr *ef;
    int te_err;

    int i;
    char **varnames, **paramnames;
    char *str_f;
    
    te_variable *allvars;

    json_t *root, *arrv, *arrp, *arrdfdx, *arrdfdp, *val;
    json_error_t json_err;
    size_t idx, nv, np;
    const char *str;
    

    root = json_load_file(fn, 0, &json_err);

    if (!root) {
        printf("Could not open JSON function file %s: %s, exiting." "\n", fn, json_err.text);
        exit(1);
    }

    /* vars */
    if (0 != json_unpack_ex(root, &json_err, 0,
                            "{s:o, s:o, s:s, s:o, s:o}",
                            "vars", &arrv,
                            "params", &arrp,
                            "f", &str_f,
                            "dfdx", &arrdfdx,
                            "dfdp", &arrdfdp
            ))
    {
        printf("JSON unpack error: %s at line %d, col %d, source %s, exiting." "\n", json_err.text, json_err.line, json_err.column, json_err.source);
        exit(1);
    }

    if (!arrv) {
        printf("No variables, exiting." "\n");
        exit(1);
    }

    if (!arrp) {
        printf("No parameters, exiting." "\n");
        exit(1);
    }
        

    /* vars & params */
    printf("Binding variables and parameters: ");
    
    nv = json_array_size(arrv);
    np = json_array_size(arrp);
    
    varnames = (char**)malloc(nv * sizeof(char*));
    paramnames = (char**)malloc(np * sizeof(char*));

    ef = (fcn_expr*)malloc(sizeof(fcn_expr));
    
    ef->nv = nv;
    ef->x = (double*)malloc(nv * sizeof(double));

    ef->np = np;
    ef->p = (double*)malloc(np * sizeof(double));

    allvars = (te_variable*)malloc((nv+np) * sizeof(te_variable));

    json_array_foreach(arrv, idx, val) {
        varnames[idx] = strdup(json_string_value(val));
        printf("%s ", varnames[idx]);
        allvars[idx].name = varnames[idx];
        allvars[idx].address = &(ef->x[idx]);
            
        /* following not used, just to silence valgrind */
        allvars[idx].type = 0;
        allvars[idx].context = NULL;
    }

    json_array_foreach(arrp, idx, val) {
        paramnames[idx] = strdup(json_string_value(val));
        printf("%s ", paramnames[idx]);
        allvars[nv + idx].name = paramnames[idx];
        allvars[nv + idx].address = &(ef->p[idx]);

        /* following not used, just to silence valgrind */
        allvars[nv + idx].type = 0;
        allvars[nv + idx].context = NULL;
    }

    printf("\n");

    /* no pcws ATM; TODO: update */
    /* vars[lvars-1].name = "pcws"; */
    /* vars[lvars-1].address = pcws; */
    /* vars[lvars-1].type = TE_FUNCTION4; */

    /* functions */
    
    /* f */
    printf("Compiling expression for f(x; p): \"%s\" ... ", str_f);
    ef->expr_f = te_compile(str_f, allvars, nv+np, &te_err);
    check_err(te_err);

    /* dfdx */
    if (json_array_size(arrdfdx) != nv) {
        printf("Numbers of variables (%zu) and function derivatives along variables (%zu) do not match!" "\n", nv, json_array_size(arrdfdx));
    }
    else {
	ef->exprs_fx = (te_expr**)malloc(nv * sizeof(te_expr*));

        json_array_foreach(arrdfdx, idx, val) {
	    str = json_string_value(val);
	    printf("Compiling expression for f_%s(x; p): \"%s\" ... ", varnames[idx], str);
	    ef->exprs_fx[idx] = te_compile(str, allvars, nv+np, &te_err);
	    check_err(te_err);
        }
    }

    /* dfdp */
    if (json_array_size(arrdfdp) != np) {
        printf("Numbers of parameters (%zu) and function derivatives along parameters (%zu) do not match!" "\n", np, json_array_size(arrdfdp));
    }
    else {
	ef->exprs_fp = (te_expr**)malloc(np * sizeof(te_expr*));

        json_array_foreach(arrdfdp, idx, val) {
	    str = json_string_value(val);
	    printf("Compiling expression for f_%s(x; p): \"%s\" ... ", paramnames[idx], str);
	    ef->exprs_fp[idx] = te_compile(str, allvars, nv+np, &te_err);
	    check_err(te_err);
        }
    }
    
    for (i=0; i< nv; i++)
        free(varnames[i]);
    free(varnames);
    for (i = 0; i< np; i++)
        free(paramnames[i]);
    free(paramnames);

    free(allvars);

    return ef;
}


void fcn_expr_free(fcn_expr *ef){

    int i;
    free(ef->x);
    free(ef->p);
    free(ef->exprs_fp);

    for (i=0;i< ef->nv; i++)
        free(ef->exprs_fx[i]);
    free(ef->exprs_fx);


    free(ef);
}

int fcn_expr_oefpil(void *data, int n, const double *x, const double *p, double *fx, double *dfdx, double *dfdp)
{
    fcn_expr *ef;
    int i, j;
    int nv, np;

    if (!data)
        return -1; /* what value? */

    ef = (fcn_expr*)data;
    nv = ef->nv;
    np = ef->np;

    /* params */
    for (j = 0; j < np; j++)
        ef->p[j] = p[j];

    for (i = 0; i < n; i++) {
        
	/* copy i-th point */
	for (j = 0; j < nv; j++)
            ef->x[j] = x[j*n + i];

        /* f */
	fx[i] = te_eval(ef->expr_f);

	/* dfdx */
	for (j = 0; j < nv; j++)
	    dfdx[j*n + i] = te_eval(ef->exprs_fx[j]);

	/* dfdp */
	for (j = 0; j < np; j++)
	    dfdp[j*n + i] = te_eval(ef->exprs_fp[j]);
    }
    
    return 0;
}

# if 0
int fcn_expr_minpack(void *data, int m, int n, const real *p, real *fvec, real *fjac, int ldfjac, int iflag)
{
    fcn_expr *ef;
    int i, j, np;

    const real *x = ((data_minpack*)data)->x;
    const real *y = ((data_minpack*)data)->y;
    ef = (fcn_expr*)((data_minpack*)data)->fcndata;

    np = ef->np;

    for (i = 0; i < ef->np; i++)
        ef->p[i] = p[i];

    if (iflag == 1) {
	/* compute residuals */
	for (i = 0; i < m; i++) {
            ef->x[i] = x[i];
	    fvec[i] = y[i] - te_eval(ef->expr_f);
        }
    }
    else {
	/* compute Jacobian */
	for (i = 0; i < ldfjac; i++) {
            ef->x[i] = x[i];
            
	    for (j = 0; j < np; j++) {
		fjac[j*ldfjac + i] = -te_eval(ef->exprs_fp[j]);
	    }
	}
    }
    
    return 0;
}

#endif
