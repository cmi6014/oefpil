add_library(minpack OBJECT
  minpack.f90
  minpack_capi.f90
)

target_include_directories(minpack PUBLIC .)
