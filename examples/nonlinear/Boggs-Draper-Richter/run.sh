#!/bin/bash

llist="0.01 0.1 0.2 0.5 1.0 2.0 5.0 10.0 100.0 1000000" 
uylist="0.1 0.5 1 2 5"

tol=1e-10
for d in $llist;do
	for uy in $uylist;do
		rm xcov ycov
		ux=$(python -c "from math import sqrt; print($uy/sqrt($d))")
		for ((i=0;i<14;i++));do
			echo -$uy >> ycov
			echo -$ux >> xcov
		done
		~/soft/oefpil/build/demo/oefpil-demo fun.json E data xcov ycov - start 4 > log_${uy}_$d
	done
done

for d in $llist;do
	for ((i=1;i<=3;i++));do
		for uy in $uylist;do
			beta=$(grep -A1 "Parameter estimates" log_${uy}_$d |tail -n1 |awk '{print $'$i'}')
			echo $beta 
		done > comparison_D${d}_b${i}
		differ=$(python -c "from numpy import loadtxt; b=loadtxt('comparison_D${d}_b${i}'); s=max(b)-min(b); print(s>$tol) ")
		if [[ $differ == "True" ]];then
			echo Problem for parameter beta$i at D=$d: minmax difference exceeded $tol
		fi
	done
done


echo '#OEFPIL D b1 b2 b3 S' > res
for d in $llist;do
	res=$(grep "residual" log_1_$d |tail -n1 |awk '{print $3}')
	echo $d $(grep -A1 "Parameter estimates" log_1_$d |tail -n1) $res
done >> res

echo "#D Boggs b1,b2,b3 OEFPIL b1,b2,b3 OEFPIL/Boggs [%]" > RESULTS.txt
paste  res.boggs res |sed -n '2,$p' |awk '{print $1,$2,$3,$4,$7,$8,$9, $7/$2*100, $8/$3*100, $9/$4*100}' >> RESULTS.txt
