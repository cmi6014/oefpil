#!/bin/bash

llist="0.01 0.1 0.2 0.5 1.0 2.0 5.0 10.0 100.0 1000000" 
uylist="0.1 0.5 1 2 5"
tol=1e-10

echo '#OEFPIL D b1 b2 b3 S' > res

for d in $llist;
do
    for uy in $uylist;
    do
        dirname=D${d}_uy${uy}
        mkdir ${dirname}
        cd ${dirname}
        
	ux=$(python3 -c "from math import sqrt; print($uy/sqrt($d))")

	for ((i=0;i<14;i++));
        do
	    echo $uy >> ycov
	    echo $ux >> xcov
	done
        
	oefpil-demo ../fun.json E ../data xcov ycov - ../start 4 > log

        for ((i=1;i<=3;i++));
        do
	    beta=$(grep -A1 "Parameter estimates" log | tail -n1 | awk '{print $'$i'}')
	    echo $beta
	done > comparison
        
	differ=$(python3 -c "from numpy import loadtxt; b=loadtxt('comparison'); s=max(b)-min(b); print(s>$tol) ")

        if [[ $differ == "True" ]];
        then
	    echo Problem for parameter beta$i at D=$d: minmax difference exceeded $tol
	fi

        cd ..
    done
done


for d in $llist;
do
    res=$(grep "residual" log_1_$d | tail -n1 | awk '{print $3}')
    echo $d $(grep -A1 "Parameter estimates" log_1_$d | tail -n1) $res
done >> res

echo "#D Boggs b1,b2,b3 OEFPIL b1,b2,b3 OEFPIL/Boggs [%]" > RESULTS.txt

paste res.boggs res | sed -n '2,$p' | awk '{print $1,$2,$3,$4,$7,$8,$9, $7/$2*100, $8/$3*100, $9/$4*100}' >> RESULTS.txt
