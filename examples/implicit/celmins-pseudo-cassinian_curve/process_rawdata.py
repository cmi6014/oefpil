#!/usr/bin/python3

import numpy as np

data=np.loadtxt("rawdata.dat")

x=data[:,0]
y=data[:,1]
n=len(x)

r2 = x**2 + y**2
phi = np.arctan(y/x)


ephi = 0.08
er = 0.02*(x**2+y**2)

Uxx = er**2*(np.cos(phi)**2) + r2*ephi**2*(np.sin(phi))**2
Uxy =  ( er**2 - r2*ephi**2)*np.sin(phi)*np.cos(phi)
Uyy = er**2*(np.sin(phi)**2) + r2*ephi**2*(np.cos(phi))**2


UU = np.zeros((2*n, 2*n))
UU[0:n,0:n] = np.diag(Uxx)
UU[0:n, n:2*n] = np.diag(Uxy)
UU[n:2*n, 0:n] = np.diag(Uxy)
UU[n:2*n, n:2*n] = np.diag(Uyy)

np.savetxt("data", np.transpose([x,y]))
np.savetxt("xcov", Uxx)
np.savetxt("ycov", Uyy)
np.savetxt("xycov", Uxy)
np.savetxt("fullcov", UU)
np.savetxt("polar.dat", np.transpose([np.sqrt(r2),np.rad2deg(phi)]))
