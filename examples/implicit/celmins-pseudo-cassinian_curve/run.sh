#!/bin/bash


../oefpil -f fcn.json -m I -d data -x xcov -y ycov -c xycov -p goodstart -v 3 > logcorrelated-var1
../oefpil -f fcn.json -m I -d data -x Uxx -y Uyy -c Uxy -p goodstart -v 3 > logcorrelated-var2
../oefpil -f fcn.json -m I -d data -x Ufull -p goodstart -v 3 > logcorrelated-var3

../oefpil -f fcn.json -m I -d data -x Unocov -p goodstart -v 3 > lognoncorr-var1
../oefpil -f fcn.json -m I -d data -x Uxx -y Uyy -p goodstart -v 3 > lognoncorr-var2


../oefpil -f fcn.json -m I -d data -x unitweight -y unitweight  -p goodstart -v 3 > logunit


