f(x,y) = 1.009447*x**2 + 0.855529*y**2 + 0.038123*x*y + 0.370761*x - 0.606626*y - 1.000000
g(x,y) = 1.045080e+00*x**2 + 8.500068e-01*y**2 - 7.060413e-02*x*y + 4.352380e-01*x - 6.206242e-01*y - 1.0

set contour base
set cntrparam levels discrete 0.0
unset surface
set isosamples 500,500

set table 'curvef.dat'
splot f(x,y)

set table 'curveg.dat'
splot g(x,y)

unset table
set term pdf
set output "hctest.pdf"
plot 'curvef.dat' w l, 'curveg.dat' w l, 'hctest' u 2:3