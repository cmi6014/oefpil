f(x,y) = 0.196746*x**2 + 0.249796*y**2 + 0.051295*x*y + 0.059811*x - 0.179526*y - 1.000000
set contour base
set cntrparam levels discrete 0.0
unset surface
set isosamples 500,500
set table 'curve.dat'
splot f(x,y)
unset table
set term pdf
set output "hctest3.pdf"
plot 'curve.dat' w l, 'hctest3' u 2:3