f(x,y) = 0.180150*x**2 + 0.228052*y**2 + 0.037117*x*y + 0.060075*x - 0.163359*y - 1.000000
set contour base
set cntrparam levels discrete 0.0
unset surface
set isosamples 500,500
set table 'curve.dat'
splot f(x,y)
unset table
set term pdf
set output "hctest2.pdf"
plot 'curve.dat' w l, 'hctest2' u 2:3