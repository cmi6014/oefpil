set term pdf

o(x) = 3.69593909e+01 * x**2 + 5.98854967e+02 * x + 7.64146326e+03 * x**(1./2) + -5.50312742e+03 * x**(1./4)
p(x) = 3.18653096e+01 * x**2 + 8.33588315e+02 * x + 3.11815682e+03 * x**(1./2) + -1.03499365e+02 * x**(1./4)
q(x) = -3.67857999e-04 * x**4 + 1.33857216e-01 * x**3 + 1.47232537e+01 * x**2 + 1.77644614e+03 * x

set xrange [0:200]

set output "Berk227-24-5-2M.pdf"
set title "Berk227-24-5-2M"

plot 'hcAp.txt' u 1:2 lw 2,\
     o(x) w l lw 2 t "OEFPIL fracpoly4 noXYcov",\
     p(x) w l lw 2 t "OEFPIL fracpoly4 XYcov",\
     q(x) w l lw 2 t "OEFPIL poly4 XYcov"
