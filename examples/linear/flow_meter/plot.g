set term pdf

f(x) = -13.9504854 * x**(-1) + -9.9896678 * x**(-0.5) + 4.8741649 - 0.1963085 * x**(0.5) + 0.0015665 * x
g(x) = -14.4747899 * x**(-1) + -9.5899811 * x**(-0.5) + 4.7952027 - 0.1928370 * x**(0.5) + 0.0015269 * x

o0(x) = x
o1(x) = -1.46957973e+01 * x**(-1) + -9.31158757e+00 * x**(-0.5) + 4.68907639e+00 + -1.80004461e-01 * x**(0.5) + 1.27252494e-03 * x
o2(x) = -1.44746862e+01 * x**(-1) + -9.59010776e+00 * x**(-0.5) + 4.79522511e+00 + -1.92837624e-01 * x**(0.5) + 1.52692359e-03 * x
o(x)  = -1.44748547e+01 * x**(-1) + -9.58998225e+00 * x**(-0.5) + 4.79520566e+00 + -1.92837185e-01 * x**(0.5) + 1.52692731e-03 * x


set xrange [-100:4000]
set yrange [-6:2.5]
set samples 10000

set output "flow-meter.pdf"
set title "Flow meter calibration (CCC software)"

plot 'data' u 1:2 lw 2,\
     f(x) w l lw 2 t "CCC 1.3",\
     g(x) w l lw 2 t "CCC 2.0",\
     o0(x) w l lw 1 t "OEFPIL start",\
     o1(x) w l lw 1 t "OEFPIL it 1",\
     o2(x) w l lw 1 t "OEFPIL it 2",\
     o(x) w l lw 2 t "OEFPIL final"
