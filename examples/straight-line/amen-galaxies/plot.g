set term pdf

f(x) = 1.1742 * x - 21.9111

o(x) = 9.60136156e-01 * x - 1.74736112e+01
p(x) = 1.17622888e+00 * x - 2.19553309e+01

set xrange [17:29]

set output "amen-galaxies.pdf"
set title "Amen Galaxies"

plot 'data' u 1:2 lw 2,\
     f(x) w l lw 2 t "Original solution",\
     o(x) w l lw 2 t "OEFPIL noXYcov",\
     p(x) w l lw 2 t "OEFPIL XYcov"
