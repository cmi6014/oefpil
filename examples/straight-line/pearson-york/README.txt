Pearson's data with York's weights

Line, cubic and exponential function, taken from WTLS paper (Malengo, Penecchi)
resp. Krystek, Anton - A weighted total...

As OEFPIL does not implement weighted regression, the weights are converted
to variances assuming w = 1/sigma^2.

See also Puchalski - A new algorithm... - Table 1 + Results

Pearson K 1901 Phil. Mag. 2 559–72
York D 1966 Can. J. Phys. 44 1079–86