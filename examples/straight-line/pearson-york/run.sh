#!/bin/bash
#
#Pearson York
../oefpil -f line-explicit.json -m E  -d data -x York-Uxx -y York-Uyy -c York-Uxy -p start-line -v 3 > log-York-explicit-var1
../oefpil -f line-explicit.json -m E  -d data -x York-Uxx -y York-Uyy  -p start-line -v 3 > log-York-explicit-var2
../oefpil -f line-explicit.json -m E  -d data -x York-Uxx -y York-Uyy  -p start-line -v 3 -t 1e-9 > log-York-explicit-var3
../oefpil -f line-explicit.json -m E  -d data -x York-Uxx -y York-Uyy  -p start-line -v 3 -r 1 > log-York-explicit-var4


#Pearson unit weights
../oefpil -f line-explicit.json -m E  -d data -x unit-weights -y unit-weights -c zero-weights -p start-line -v 3  > log-unit-explicit-var1
../oefpil -f line-explicit.json -m E  -d data -x unit-weights -y unit-weights  -p start-line -v 3  > log-unit-explicit-var2
../oefpil -f line-explicit.json -m E  -d data -x unit-weights -y unit-weights  -p start-line -v 3 -r 1  > log-unit-explicit-var3
../oefpil -f line-explicit.json -m E  -d data -x unit-weights -y unit-weights  -p start-line -v 3 -r 6  > log-unit-explicit-var4



#higher order polynomials
../oefpil -f cubic.json -m E -d data -x xcov -y ycov -p start-cubic -v 3 > log_cubic_York
../oefpil -f quintic.json -m E -d data -x xcov -y ycov -p start-quintic -v 3 > log_quintic_York
../oefpil -f cubic.json -m E -d data -x unit-weights -y unit-weights -p start-cubic -v 3 > log_cubic_unitweights
../oefpil -f quintic.json -m E -d data -x unit-weights -y unit-weights -p start-quintic -v 3 > log_quintic_unitweights

