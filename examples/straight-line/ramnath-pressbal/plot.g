set term pdf

w(x) = 1.9614442200 * (1 + 0.0599991615 * x)
o(x) = 1.96144399 * (1 + 6.06739290e-08 * x)

set xrange [0:550]
set yrange [1.96143:1.96152]
set samples 10000

set output "pressbal.pdf"
set title "Pressure balance calibration (WTLSC)"

plot 'data' u 1:2 lw 2,\
     o(x) w l lw 2 t "OEFPIL",\
     w(x) w l lw 2 t "WTLSC"