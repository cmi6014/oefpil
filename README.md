# OEFPIL

Optimum Estimate of Function Parameters by Iterated Linearization

# Authors

Gejza Wimmer (Masaryk University)
Zdeňka Geršlová (Masaryk University)
Vojtěch Šindlář (Masaryk University)
Radek Šlesinger (Czech Metrology Institute)
Anna Charvátová Campbell (Czech Metrology Institute)

# Contact

rslesinger@cmi.gov.cz

# Acknowledgement

This software was financed by the Technology Agency of the Czech Republic within the Zéta Programme.

# OEFPIL for R

There is also a variant of OEFPIL for R available at https://cran.r-project.org/package=OEFPIL.
