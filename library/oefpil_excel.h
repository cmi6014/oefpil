#ifndef OEFPIL_EXCEL_H
#define OEFPIL_EXCEL_H

typedef struct oefpil_type_t
{
    long ndata;
    long np;
    double* x;
    double* y;
    double* ux;
    double* uy;
    double* xtrue;
    double* ytrue;    
    double* params;
    double* pcov;
} oefpil_type;

#pragma once

#ifdef _MSC_VER

#ifdef OEFPIL_EXPORTS
#define OEFPIL_API __declspec(dllexport)
#else
#define OEFPIL_API __declspec(dllimport)
#endif

//long __stdcall bla6(long np, double* params, double* pcov);

//extern OEFPIL_API long bla6(long np, double* params, double* pcov);

//extern long __stdcall bla7(double* params, double* pcov);

//extern OEFPIL_API long bla8(double* n1, double* n2, double* n3, double* n4, double* n5, double* n6, double* n7, double* n8, double* n9, double* n10);

extern OEFPIL_API long __stdcall oefpilexcel(long ndata, double* x, double* y, double* ux, double* uy, double* xtrue, double* ytrue, long np, double* params, double* pcov);

extern OEFPIL_API long oefpilexcel1(oefpil_type* t);

//extern OEFPIL_API long oefpilexcel(double* x, double* y, double* ux, double* uy, double* xtrue, double* ytrue, double* params, double* pcov, long ndata, long np);



//extern OEFPIL_API long __stdcall oefpilexcel(double* x, double* y, double* ux, double* uy, double* xtrue/*, double* ytrue, double* params, double* pcov*/);

//long __stdcall oefpilexcel(double* x/*, double* y, double* ux, double* uy/*, double* xtrue/*, double* ytrue, double* params, double* pcov*/);

//extern OEFPIL_API long __stdcall oefpilexcel(double* x/*, double* y, double* ux, double* uy/*, double* xtrue/*, double* ytrue, double* params, double* pcov*/);

//extern OEFPIL_API long __stdcall oefpilexcel(double* x, double* y, double* ux, double* uy, double* xtrue, double* ytrue, double* params, double* pcov);  //********************************************

//extern OEFPIL_API long __cdecl oefpilexcel(double* x, double* y, double* ux, double* uy, double* xtrue/*, double* ytrue, double* params, double* pcov*/);


extern OEFPIL_API long bla4(double* arr);



//extern OEFPIL_API long oefpilexcel(long* pndata, double* x, double* y, double* ux, double* uy, double* xtrue, double* ytrue, long* pnp, double* params, double* pcov);

//extern OEFPIL_API long oefpilexcel(double* x, double* y, double* ux, double* uy, double* xtrue, double* ytrue, double* params, double* pcov, long ndata, long np);

//extern OEFPIL_API long oefpilexcel(long ndata, long np, long nc, double* x, double* y, double* ux, double* uy, double* xtrue, double* ytrue, double* params, double* pcov);


//long __stdcall oefpilexcel(long ndata, double* x, double* y, double* ux, double* uy, double* xtrue, double* ytrue, long np, double* params, double* pcov);
#endif


//long oefpilexcel(long ndata, double* x, double* y, double* ux, double* uy, double* xtrue, double* ytrue, long np, double* params, double* pcov);



#endif
