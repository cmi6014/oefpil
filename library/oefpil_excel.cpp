#include "oefpil_excel.h"
#include "oefpil.h"

#include <float.h> /* DBL_EPSILON */
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#ifdef _WIN32
#include <windows.h>
#include <stddef.h>
#else
#include <unistd.h>
#endif


double line(double x, const double *p)
{
    double res;

    res = p[0] *x + p[1];

    return res; 
}

static double line_dx(double x, const double *p)
{
    double res;

    res = p[0];

    return res;
}

static double line_dp(double x, const double *p, int i)
{
    double res;

    if (i == 0)
        res = x;
    else if (i==1)
        res = 1;

    return res;
}


int fcn_oefpil_line(void *data, int n, const double *x, const double *p, double *wf, double *wfx, double *wfp)
{
    int i, j;
    int np = 2;

    for (i = 0; i < n; i++){
        wf[i] = line(x[i], p);
    }

    for (i = 0; i < n; i++){
        wfx[i] = line_dx(x[i], p);
    }

    for (i = 0; i < n; i++) {
        for (j = 0; j < np; j++) {
            wfp[j*n + i] = line_dp(x[i], p, j);
        }
    }

    return 0;

}

void leastsquares ( int ndata, double *x, double *y, int np, double *params){
	double sxx=0, sxy=0;
	double sx=0, sy=0;
	int i;

	for (i=0;i< ndata;i++){
		sxx += x[i]*x[i];
		sxy += x[i]*y[i];
		sx += x[i];
		sy += y[i];
	}
	
	params[1] = (sy * sxx- sx*sxy)/(ndata*sxx - sx*sx);
	params[0] = (ndata*sxy -sx *sy)/(ndata*sxx - sx*sx);

}


void leastsquares_unc ( int ndata, double *x, double *y, double *uy, int np, double *params, double *pcov){
	double sxx=0, sxy=0, invuy=0;
	double sx=0, sy=0;
	double suu=0;
	int i;

	for (i=0;i< ndata;i++){
		sxy += x[i]*y[i]/(uy[i]*uy[i]);
		sy += y[i]/(uy[i]*uy[i]);
		sx += x[i];
		sxx += x[i]*x[i];
		invuy += 1./(uy[i]*uy[i]);
		suu += uy[i]*uy[i];
	}
	
	params[0] = (sxy -sx *sy)/(ndata*sxx - sx*sx)/invuy;
	params[1] = (sy * sxx- sx*sxy)/(ndata*sxx - sx*sx)/invuy;



	/*TODO */
	pcov[0] = sqrt(ndata/(ndata-2) * suu/(ndata*sxx- sx*sx));
	pcov[1] = 0;
	pcov[2] = pcov[0]*sqrt(1./ndata*sxx);
	pcov[3] = 0;

}

void bla5(int ndata, double* x, double* y, double* ux, double* uy, double* xtrue, double* ytrue, int np, double* params, double* pcov)
{
	return;
}

void oefpilexcel(int ndata, double *x, double *y, double *ux, double *uy, double *xtrue, double *ytrue,  int np, double *params, double *pcov)
{
	return;

	int i;
	int tilingmode;
	int *tilemap;
	bool uxiszero, uyiszero;
	double tol;
	void *fcn_oefpil;
	double  *K, *Kxx, *Kyy;

	/* zatim jen primka */
	fcn_oefpil = fcn_oefpil_line;

	/* sestaveni kovariancni matice */
	Kxx = (double *)malloc(ndata *sizeof(double));
	Kyy = (double*)malloc(ndata * sizeof(double));
	for (i = 0; i < ndata; i++){
		Kxx[i] = ux[i]*ux[i];
		Kyy[i] = uy[i]*uy[i];
	}
	tilingmode = TILING_DIAG;
	tilemap = oefpil_tilemap_diagtiles_new(2);
	K = oefpil_tcm_diag_new(ndata, 2, tilemap);
	oefpil_tcm_diag_set_tile_diag(ndata, 2, K, tilemap, 0, Kxx);
	oefpil_tcm_diag_set_tile_diag(ndata, 2, K, tilemap, 1, Kyy);

	/* pocatecni odhad pro primku zadame natvrdo */
	params[0] = 1;
	params[1] = 1;

	/* vystup skutecne hodnoty  */
	for (i=0;i< ndata; i++){
		xtrue[i] = x[i];
		ytrue[i] = y[i];
	}

	/* tolerance */
	tol = exp(log(DBL_EPSILON)  * 2.0/3); /* DBL_EPSILON^2/3 as used in ODRPACK; likely unreasonably strict */

	/* zjistit jestli jsou nenulove nejistoty y */
	uyiszero = false;
	for (i=0; i< ndata; i++){
		if (uy[i] == 0)
			uyiszero = true;
	}
	/* zjistit jestli jsou nenulove nejistoty x */
	uxiszero = false;
	for (i=0; i< ndata; i++){
		if (ux[i] == 0)
			uxiszero = true;
	}  

	/*pokud jsou nejistoty nulove, pouzit obycejnou metodu nejmensich ctvercu */
	if (uxiszero){
		if (uyiszero)
			leastsquares(ndata, x,y,np, params);
		else
			leastsquares_unc(ndata, x,y, uy,np, params, pcov);

	}
	else{
		oefpil(fcn_oefpil, NULL, FUNCTION_EXPLICIT, np, params, pcov, ndata, 1, x, y, xtrue, ytrue, K, tilingmode, tilemap, 50, tol,
				0, stdout, NULL, REL_PAR_OR_ABS_PAR_AND_REL_X_OR_ABS_X, NULL, NULL, NULL, false, NULL);
	}


	free(Kxx);
	free(Kyy);
	free(tilemap);
	free(K);
}
