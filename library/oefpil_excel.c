#include "oefpil_excel.h"
#include "oefpil.h"

#include <float.h> /* DBL_EPSILON */
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif


double line(double x, const double *p)
{
    double res;

    res = p[0] *x + p[1];

    return res; 
}

static double line_dx(double x, const double *p)
{
    double res;

    res = p[0];

    return res;
}

static double line_dp(double x, const double *p, int i)
{
    double res;

    if (i == 0)
        res = x;
    else if (i==1)
        res = 1;

    return res;
}


int fcn_oefpil_line(void *data, int n, const double *x, const double *p, double *wf, double *wfx, double *wfp)
{
    int i, j;
    int np = 2;

    for (i = 0; i < n; i++){
        wf[i] = line(x[i], p);
    }

    for (i = 0; i < n; i++){
        wfx[i] = line_dx(x[i], p);
    }

    for (i = 0; i < n; i++) {
        for (j = 0; j < np; j++) {
            wfp[j*n + i] = line_dp(x[i], p, j);
        }
    }

    return 0;

}

void leastsquares ( int ndata, double *x, double *y, int np, double *params){
	double sxx=0, sxy=0;
	double sx=0, sy=0;
	int i;

	for (i=0;i< ndata;i++){
		sxx += x[i]*x[i];
		sxy += x[i]*y[i];
		sx += x[i];
		sy += y[i];
	}
	
	params[1] = (sy * sxx- sx*sxy)/(ndata*sxx - sx*sx);
	params[0] = (ndata*sxy -sx *sy)/(ndata*sxx - sx*sx);

}


void leastsquares_unc ( int ndata, double *x, double *y, double *uy, int np, double *params, double *pcov){
	double sxx=0, sxy=0, invuy=0;
	double sx=0, sy=0;
	double suu=0;
	int i;

	for (i=0;i< ndata;i++){
		sxy += x[i]*y[i]/(uy[i]*uy[i]);
		sy += y[i]/(uy[i]*uy[i]);
		sx += x[i];
		sxx += x[i]*x[i];
		invuy += 1./(uy[i]*uy[i]);
		suu += uy[i]*uy[i];
	}
	
	params[0] = (sxy -sx *sy)/(ndata*sxx - sx*sx)/invuy;
	params[1] = (sy * sxx- sx*sxy)/(ndata*sxx - sx*sx)/invuy;



	/*TODO */
	pcov[0] = sqrt(ndata/(ndata-2) * suu/(ndata*sxx- sx*sx));
	pcov[1] = 0;
	pcov[2] = pcov[0]*sqrt(1./ndata*sxx);
	pcov[3] = 0;

}

int bla6(int np, double* params, double* pcov)
{
	return np;

	for (int i = 0; i < 2; i++)
	{
		params[i] = 100;
	}
	
	
	for (int i = 0; i < 2; i++)
	{
		pcov[i] = 200;
	}


	return 111;
}

int bla7(double* params, double* pcov)
{
	params[0]+=10;
	pcov[0] += 20;
	return (int)(params[0]);
}

int bla8(double* n1, double* n2, double* n3, double* n4, double* n5, double* n6, double* n7, double* n8, double* n9, double* n10)
{
	return (int)n9[0];
}

long bla4(double* arr)
{
	//return 12;

	if (NULL == arr)
		return 0;

	arr[0] *= 2;
	arr[1] *= 2;
	arr[2] *= 2;
	arr[3] *= 2;

	return 555;
}

long oefpilexcel1(oefpil_type* t)
{
	t->x[1] += 123.123;
	return (long)t->x[1];

	//return oefpilexcel(t->ndata, t->x, t->y, t->ux, t->uy, t->xtrue, t->ytrue, t->np, t->params, t->pcov);
}

// returns 0 when succeeded; otherwise returns 1
// 
//int oefpilexcel(double* x, double* y, double* ux, double* uy, double* xtrue, double* ytrue, double* params, double* pcov, int ndata, int np)
// 
//int oefpilexcel(double* x, double* y, double* ux, double* uy, double* xtrue, double* ytrue, double* params, double* pcov, int ndata, int np)
// 
//int oefpilexcel(int* pndata, double* x, double* y, double* ux, double* uy, double* xtrue, double* ytrue, int* pnp, double* params, double* pcov)


//long oefpilexcel(double* x, double* y, double* ux, double* uy, double* xtrue, double* ytrue, double* params, double* pcov)

long oefpilexcel(int ndata, double* x, double* y, double* ux, double* uy, double* xtrue, double* ytrue, int np, double* params, double* pcov)
{
#if(0)
	x[0] *= 2;
	x[1] *= 2;
	x[2] *= 2;

	y[0] *= 2;
	y[1] *= 2;
	y[2] *= 2;

	ux[0] *= 2;
	ux[1] *= 2;
	ux[2] *= 2;

	uy[0] *= 2;
	uy[1] *= 2;
	uy[2] *= 2;

	xtrue[0] *= 2;
	xtrue[1] *= 2;
	xtrue[2] *= 2;

	ytrue[0] *= 2;
	ytrue[1] *= 2;
	ytrue[2] *= 2;

	params[0] *= 2;
	params[1] *= 2;
	params[2] *= 2;

	pcov[0] *= 2;
	pcov[1] *= 2;
	pcov[2] *= 2;

	return 123;
#endif


//#pragma comment(linker, "/EXPORT:" __FUNCTION__ "=" __FUNCDNAME__)

	//int ndata = (int)desc[0];
	//int np = (int)desc[1];
	// 
	//int ndata = 10;
	//int np = 2;

	//x[0] = 100;

	//return 123;

	// 
	//return np;

	//int ndata = pndata[0];
	//int np = pnp[0];


	//if (NULL == x || NULL == y || NULL == ux || NULL == uy || NULL == xtrue || NULL == ytrue || NULL == params || NULL == pcov) {		
	//	return 1;
	//}

/*	if (NULL == x)
		return 10;
	if (NULL == y)
		return 11;
	if (NULL == ux)
		return 12;
	if (NULL == uy)
		return 13;
	if (NULL == xtrue)
		return 14;
	if (NULL == ytrue)
		return 15;
	if (NULL == params)
		return 16;
	if (NULL == pcov)
		return 17;	*/

#if(0)
	for (int i = 0; i<ndata; i++)
	{
		x[i] += 10;
		y[i] += 10;
		ux[i] += 10;
		uy[i] += 10;
		xtrue[i] += 10;
		ytrue[i] += 10;
	}	

	for (int i = 0; i<np; i++)
	{
		params[i] += 10;
	}
	
	
	for (int i = 0; i<np*np; i++)
	{
		pcov[i] += 10;
	}

	return 0;
#endif

#if(1)
	int i;
	int tilingmode;
	int *tilemap;
	bool uxiszero, uyiszero;
	double tol;
	void *fcn_oefpil;
	double  *K, *Kxx, *Kyy;

	/* zatim jen primka */
	fcn_oefpil = fcn_oefpil_line;

	/* sestaveni kovariancni matice */
	Kxx = (double *)malloc(ndata *sizeof(double));
	Kyy = (double*)malloc(ndata * sizeof(double));
	for (i = 0; i < ndata; i++){
		Kxx[i] = ux[i]*ux[i];
		Kyy[i] = uy[i]*uy[i];
	}
	tilingmode = TILING_DIAG;
	tilemap = oefpil_tilemap_diagtiles_new(2);
	K = oefpil_tcm_diag_new(ndata, 2, tilemap);
	oefpil_tcm_diag_set_tile_diag(ndata, 2, K, tilemap, 0, Kxx);
	oefpil_tcm_diag_set_tile_diag(ndata, 2, K, tilemap, 1, Kyy);

	/* pocatecni odhad pro primku zadame natvrdo */
	params[0] = 1;
	params[1] = 1;

	/* vystup skutecne hodnoty  */
	for (i=0;i< ndata; i++){
		xtrue[i] = x[i];
		ytrue[i] = y[i];
	}

	/* tolerance */
	tol = exp(log(DBL_EPSILON)  * 2.0/3); /* DBL_EPSILON^2/3 as used in ODRPACK; likely unreasonably strict */

	/* zjistit jestli jsou nenulove nejistoty y */
	uyiszero = false;
	for (i=0; i< ndata; i++){
		if (uy[i] == 0)
			uyiszero = true;
	}
	/* zjistit jestli jsou nenulove nejistoty x */
	uxiszero = false;
	for (i=0; i< ndata; i++){
		if (ux[i] == 0)
			uxiszero = true;
	}  

	/*pokud jsou nejistoty nulove, pouzit obycejnou metodu nejmensich ctvercu */
	if (uxiszero){
		if (uyiszero)
			leastsquares(ndata, x,y,np, params);
		else
			leastsquares_unc(ndata, x,y, uy,np, params, pcov);

	}
	else{
		oefpil(fcn_oefpil, NULL, FUNCTION_EXPLICIT, np, params, pcov, ndata, 1, x, y, xtrue, ytrue, K, tilingmode, tilemap, 50, tol,
				0, stdout, NULL, REL_PAR_OR_ABS_PAR_AND_REL_X_OR_ABS_X, NULL, NULL, NULL, false, NULL);
	}


	free(Kxx);
	free(Kyy);
	free(tilemap);
	free(K);
#endif

	return 0;
}
